var matrix = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[5,5,0,0,0,0,0,0,0,0,0,0,0,10,1,0,0,7],[2,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0],[2,0,0,0,3,0,0,5,5,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,1],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[1,0,0,0,0,0,0,0,0,0,6,0,0,7,0,0,0,0]];
var packages = [{
"name": " com.carlosgub.asistenciaulima.domain.model", "color": " #3182bd"
}
,{
"name": " com.carlosgub.asistenciaulima.presentation.adapters", "color": " #6baed6"
}
,{
"name": " com.carlosgub.asistenciaulima.data.sources", "color": " #9ecae1"
}
,{
"name": " com.carlosgub.asistenciaulima.utils.dialogs", "color": " #c6dbef"
}
,{
"name": " com.carlosgub.asistenciaulima.data.mapper", "color": " #e6550d"
}
,{
"name": " com.carlosgub.asistenciaulima.utils", "color": " #fd8d3c"
}
,{
"name": " com.carlosgub.asistenciaulima.utils.glide", "color": " #fdae6b"
}
,{
"name": " com.carlosgub.asistenciaulima.domain.repository", "color": " #fdd0a2"
}
,{
"name": " com.carlosgub.asistenciaulima.data.sources.firebase", "color": " #31a354"
}
,{
"name": " com.carlosgub.asistenciaulima.presentation.views", "color": " #74c476"
}
,{
"name": " com.carlosgub.asistenciaulima.domain.usecases", "color": " #a1d99b"
}
,{
"name": " com.carlosgub.asistenciaulima.data.repository", "color": " #c7e9c0"
}
,{
"name": " com.carlosgub.asistenciaulima.data", "color": " #756bb1"
}
,{
"name": " com.carlosgub.asistenciaulima.presentation", "color": " #9e9ac8"
}
,{
"name": " com.example.carlosgub.dispensadodecomida.utils.extensions", "color": " #bcbddc"
}
,{
"name": " com.carlosgub.asistenciaulima", "color": " #dadaeb"
}
,{
"name": " com.carlosgub.asistenciaulima.utils.notifications", "color": " #636363"
}
,{
"name": " com.carlosgub.asistenciaulima.presentation.presenters", "color": " #969696"
}
];
