package com.carlosgub.asistenciaulima.presentation.adapters

import android.preference.PreferenceManager
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.utils.glide.GlideApp
import kotlinx.android.synthetic.main.item_asesorias.view.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor


class AsesoriasRecyclerAdapter(private val chatList: List<PeticionAsesoria>, private val activity:FragmentActivity, private var chatListFiltered:List<PeticionAsesoria>, private var callback:(Boolean) -> Unit, private var callbackClick: (View, PeticionAsesoria,String) ->Unit, private var callbackOnLongPress: (View, PeticionAsesoria,String) -> Unit) : RecyclerView.Adapter<AsesoriasRecyclerAdapter.ViewHolder>(),Filterable {

    override fun getItemCount():Int=chatListFiltered.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_asesorias, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            if(!chatListFiltered.isEmpty()){
                callback(true)
                val asesoria = chatListFiltered[position]
                val type = PreferenceManager.getDefaultSharedPreferences(context).getString("userType", "")
                if(type=="Alumno"){
                    teacherNameIA.text=asesoria.nameProfesor
                    GlideApp.with(context)
                            .load(asesoria.imageUrl)
                            .error(R.drawable.avatar)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .apply(RequestOptions.circleCropTransform())
                            .into(avatarIA)
                }else{
                    teacherNameIA.text=asesoria.nameAlumno
                }
                fechaIA.text = "${asesoria.dia} - ${asesoria.hora}"
                lugarIA.text = asesoria.lugar

                if(asesoria.estado=="ENVIADO"){
                    estadoIA.text = resources.getText(R.string.estado_pendiente_de_aceptacion)
                    estadoIA.backgroundResource = R.drawable.background_estado_pendiente
                    estadoIA.textColor = resources.getColor(R.color.black)
                }else if(asesoria.estado == "ACEPTADO"){
                    estadoIA.text = resources.getText(R.string.estado_aceptado)
                    estadoIA.backgroundResource = R.drawable.background_estado_aceptado
                    estadoIA.textColor = resources.getColor(R.color.white)
                }else if(asesoria.estado == "RECHAZADO"){
                    estadoIA.text = resources.getText(R.string.estado_rechazado)
                    estadoIA.backgroundResource = R.drawable.background_estado_terminado
                    estadoIA.textColor = resources.getColor(R.color.white)
                }else if(asesoria.estado == "TERMINADO"){
                    estadoIA.text = resources.getText(R.string.estado_terminado)
                    estadoIA.backgroundResource = R.drawable.background_estado_terminado
                    estadoIA.textColor = resources.getColor(R.color.white)
                }

                cardViewIA.setOnClickListener {callbackClick(it,asesoria,type) }

                cardViewIA.setOnLongClickListener {
                    callbackOnLongPress(it,asesoria,type)
                true
                }
            }else{
                callback(false)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    chatListFiltered = chatList
                } else {
                    val filteredList = mutableListOf<PeticionAsesoria>()
                    for (row in chatList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.nameProfesor.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    chatListFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = chatListFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                chatListFiltered = filterResults.values as List<PeticionAsesoria>
                if(chatListFiltered.isEmpty()) callback(false)
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}