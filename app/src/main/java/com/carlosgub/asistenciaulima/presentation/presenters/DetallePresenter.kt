package com.carlosgub.asistenciaulima.presentation.presenters

import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import javax.inject.Inject


class DetallePresenter @Inject constructor(): BasePresenter {

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
    }

    interface View: BaseView {
    }

}