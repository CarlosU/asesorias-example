package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import com.carlosgub.asistenciaulima.BuildConfig
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Documento
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.adapters.DetalleActivityRecyclerAdapter
import com.carlosgub.asistenciaulima.presentation.presenters.DetallePresenter
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_detalle.*
import org.jetbrains.anko.backgroundResource
import org.jetbrains.anko.textColor
import org.jetbrains.anko.toast
import java.io.File
import javax.inject.Inject


class DetalleActivity : BaseActivity(),DetallePresenter.View {

    @Inject lateinit var mPresenter:DetallePresenter
    private lateinit var mLayoutManager:StaggeredGridLayoutManager
    private lateinit var adapter: DetalleActivityRecyclerAdapter

    private val STORAGE_REQUEST_CODE = 101

    lateinit var peticionAsesoria: PeticionAsesoria

    lateinit var doc:Documento

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setupWindowAnimations()
        setSupportActionBar(toolbarAD)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        val intent = intent
        peticionAsesoria = intent.getSerializableExtra("peticionAsesoria") as PeticionAsesoria
        val type = PreferenceManager.getDefaultSharedPreferences(applicationContext).getString("userType", "")
        if(type=="Alumno"){
            tvnombreAD.text = peticionAsesoria.nameProfesor
        }else{
            tvnombreAD.text =peticionAsesoria.nameAlumno
        }

        tvDiaAD.text = "${peticionAsesoria.dia} (${peticionAsesoria.hora})"
        tvLugarAD.text = peticionAsesoria.lugar
        tvAsuntoAD.text = peticionAsesoria.tema
        if(peticionAsesoria.estado=="ENVIADO"){
            tvEstadoAD.text = resources.getText(R.string.estado_pendiente_de_aceptacion)
            tvEstadoAD.backgroundResource = R.drawable.background_estado_pendiente
            tvEstadoAD.textColor = resources.getColor(R.color.black)
        }else if(peticionAsesoria.estado == "ACEPTADO"){
            tvEstadoAD.text = resources.getText(R.string.estado_aceptado)
            tvEstadoAD.backgroundResource = R.drawable.background_estado_aceptado
            tvEstadoAD.textColor = resources.getColor(R.color.white)
        }else if(peticionAsesoria.estado == "RECHAZADO"){
            tvEstadoAD.text = resources.getText(R.string.estado_rechazado)
            tvEstadoAD.backgroundResource = R.drawable.background_estado_terminado
            tvEstadoAD.textColor = resources.getColor(R.color.white)
        }else if(peticionAsesoria.estado == "TERMINADO"){
            tvEstadoAD.text = resources.getText(R.string.estado_terminado)
            tvEstadoAD.backgroundResource = R.drawable.background_estado_terminado
            tvEstadoAD.textColor = resources.getColor(R.color.white)
        }

        //Adapter
        adapter = DetalleActivityRecyclerAdapter(peticionAsesoria.documentos,{
            val rootPath = File(Environment.getExternalStorageDirectory(), "Asesorias Ulima")
            if (!rootPath.exists()) {
                toast("Descargue primero el documento, realize long press")
                return@DetalleActivityRecyclerAdapter
            }
            val asesoriaPath = File(rootPath, peticionAsesoria.id)
            if (!asesoriaPath.exists()) {
                toast("Descargue primero el documento, realize long press")
                return@DetalleActivityRecyclerAdapter
            }
            val localFile = File(asesoriaPath, "${it.name_archivo}.${it.tipo_archivo}")
            if(localFile.exists()){
                openFile(localFile)
            }else{
                toast("Descargue primero el documento")
            }
        },{
            doc = it
            setupPermissions()
        })
        mLayoutManager = StaggeredGridLayoutManager(1, 1)
        rvAD.layoutManager = mLayoutManager
        rvAD.visibility= View.VISIBLE
        rvAD.adapter = adapter


    }

    fun downloadFile(){
        val storage = FirebaseStorage.getInstance()
        val storageRef = storage.getReferenceFromUrl(doc.url_archivo)
        val rootPath = File(Environment.getExternalStorageDirectory(), "Asesorias Ulima")
        if (!rootPath.exists()) {
            rootPath.mkdirs()
        }
        val asesoriaPath = File(rootPath, peticionAsesoria.id)
        if (!asesoriaPath.exists()) {
            asesoriaPath.mkdirs()
        }
        val localFile = File(asesoriaPath, "${doc.name_archivo}.${doc.tipo_archivo}")
        if (!localFile.exists()) {
            storageRef.getFile(localFile).addOnSuccessListener {
                openFile(localFile)
                //  updateDb(timestamp,localFile.toString(),position);
            }.addOnFailureListener { exception ->
                toast("local tem file not created  created \" + $exception")
            }
        }else{
            toast("Ya lo haz descargado, solo haga tap")
        }
    }

    override val layout: Int get() = R.layout.activity_detalle
    override val presenter: BasePresenter? get() = mPresenter

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showMessage(title: String, message: String) {
    }

    /*private fun setupWindowAnimations() {
        val slide = Slide(Gravity.AXIS_PULL_BEFORE)
        slide.duration = 1000
        window.exitTransition = slide
    }*/

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    STORAGE_REQUEST_CODE)
        }else{
            downloadFile()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            STORAGE_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    toast("Necesita aceptar el permiso de almacenamiento")
                    setupPermissions()
                } else {
                    downloadFile()
                }
            }
        }
    }

    fun openFile(url:File) {

        val uri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider",
                url.absoluteFile)

        val intent = Intent(Intent.ACTION_VIEW)

        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*")
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)

    }
}
