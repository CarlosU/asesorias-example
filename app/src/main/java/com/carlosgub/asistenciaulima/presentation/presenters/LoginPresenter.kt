package com.carlosgub.asistenciaulima.presentation.presenters

import android.content.Context
import android.preference.PreferenceManager
import com.carlosgub.asistenciaulima.domain.usecases.GetUserUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import com.kelvinapps.rxfirebase.RxFirebaseAuth
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject


class LoginPresenter  @Inject constructor(var view: View,
                                          private val useCase: GetUserUseCase): BasePresenter {
    private val cb = CompositeSubscription()
    private lateinit var mContext: Context

    fun context(context:Context){
        mContext=context
    }

    fun login(email:String, pswd:String) {
        view.showProgress()
        if(verificarCampos(email,pswd)){
            cb.add(RxFirebaseAuth.signInWithEmailAndPassword(FirebaseAuth.getInstance(),email,pswd)
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        cb.add(useCase.getUserType(it.user.uid)
                                .subscribeOn(Schedulers.io())
                                .subscribe {userType->
                                    FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                                        FirebaseDatabase.getInstance().reference.child("/user/${it.user.uid}/notificationKey").setValue(instanceIdResult.token)
                                        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("userType", userType).apply()
                                        view.hideProgress()
                                        view.loginCorrecto()
                                    }
                                })

                    },{
                        view.hideProgress()
                        view.loginIncorrecto("Usuario o clave Incorrecta")
                    }))
        }else{
            view.hideProgress()
        }

    }

    fun verificarCampos(email:String, pswd:String):Boolean{
        return if(!email.isNullOrBlank() && !email.isNullOrEmpty()){
            if(!pswd.isNullOrBlank() && !pswd.isNullOrEmpty()){
                true
            }else{
                view.loginIncorrecto("Ingrese la constrasenia")
                false
            }
        }else{
            view.loginIncorrecto("Ingrese un usuario")
            false
        }
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        cb.clear()
    }

    interface View: BaseView {
        fun loginCorrecto()
        fun loginIncorrecto(msg:String)
    }

}