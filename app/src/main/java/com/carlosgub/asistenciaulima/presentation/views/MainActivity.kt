package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.presenters.MainPresenter
import com.example.carlosgub.dispensadodecomida.utils.extensions.replaceContentFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import javax.inject.Inject


class MainActivity : BaseActivity() , MainPresenter.View{

    @Inject lateinit var mPresenter: MainPresenter

    private val CAMERA_REQUEST_CODE = 101

    private lateinit var mMenu:Menu


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbarAM)

        mPresenter.prepareToShow(R.id.chat)
        bottomNavigationAM.setOnNavigationItemSelectedListener {
            mPresenter.prepareToShow(it.itemId)
        }
    }

    override val layout: Int get() = R.layout.activity_main

    override val presenter: BasePresenter? get() = mPresenter


    override fun showFragmentView(fragment: Fragment) {
        replaceContentFragment(R.id.layoutContentAM, fragment)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        mMenu = menu
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_toolbar_main, menu)
        val type = PreferenceManager.getDefaultSharedPreferences(applicationContext).getString("userType", "")
        if(type=="Profesor") menu.findItem(R.id.qr).isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId){
            R.id.logout-> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this,LoginActivity::class.java))
                finish()
                true
            }
            R.id.qr->{
                setupPermissions()
                true
            }
            /*R.id.search->{
                true
            }*/
            else-> super.onOptionsItemSelected(item)
        }

    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.CAMERA),
                    CAMERA_REQUEST_CODE)
        }else{
            scanQr()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                             permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    toast("Necesita aceptar el permiso de la camara")
                    setupPermissions()
                } else {
                    scanQr()
                }
            }
        }
    }

    private fun scanQr(){
        IntentIntegrator(this).initiateScan()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents != null) {
                mPresenter.createChat(result.contents)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun showProgress() {
        startActivity(Intent(applicationContext,CargandoActivity::class.java))
    }

    override fun hideProgress() {
        CargandoActivity.fa.finish()
    }

    override fun showMessage(msg: String) {
        toast(msg)
    }

    override fun onBackPressed() {
        val menuSearch = mMenu.findItem(R.id.search)
        val searchView = menuSearch.actionView as SearchView
        searchView.setQuery("", false)
        searchView.isIconified = true
        menuSearch.collapseActionView()
    }

    /*@SuppressLint("PrivateResource")
    fun circleReveal(viewID: Int, posFromRight: Int, containsOverflow:Boolean, isShow:Boolean){
        val myView:View = findViewById(viewID)
        var width = myView.width

        if(posFromRight>0)
            width(<span class="pl-k">-=</span>(posFromRight<span class="pl-k">*</span>getResources()<span class="pl-k">.</span>getDimensionPixelSize(<span class="pl-smi">R</span><span class="pl-k">.</span>dimen<span class="pl-k">.</span>abc_action_button_min_width_material))<span class="pl-k">-</span>(getResources()<span class="pl-k">.</span>getDimensionPixelSize(<span class="pl-smi">R</span><span class="pl-k">.</span>dimen<span class="pl-k">.</span>abc_action_button_min_width_material)<span class="pl-k">/</span> <span class="pl-c1">2</span>);
        if(containsOverflow)
            width-= resources.getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material)

        var cx = width
        var cy = myView.height/2

        var anim:Animator

        anim = if(isShow)
            ViewAnimationUtils.createCircularReveal(myView,cx,cy,0F,width.toFloat())
        else
            ViewAnimationUtils.createCircularReveal(myView, cx, cy, width.toFloat(), 0f)

        anim.duration = 220.toLong()

        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                if (!isShow) {
                    super.onAnimationEnd(animation)
                    myView.visibility = View.INVISIBLE
                }
            }
        })

        // make the view visible and start the animation
        if(isShow)
            myView.visibility = View.VISIBLE

        // start the animation
        anim.start()
    }*/

}
