package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.google.firebase.auth.FirebaseAuth

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scheduleSplashScreen()

    }

    override val layout: Int = R.layout.activity_splash
    override val presenter: BasePresenter? = null

    private fun scheduleSplashScreen() {
        Handler().postDelayed({
            var mAuth = FirebaseAuth.getInstance().currentUser

            intent = if(mAuth==null){
                Intent(this, LoginActivity::class.java)
            }else{
                Intent(this, MainActivity::class.java)
            }
            startActivity(intent)
        },1000
        )
    }
}
