package com.carlosgub.asistenciaulima.presentation.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.toast
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.carlosgub.asistenciaulima.domain.model.AsesoriaList
import com.carlosgub.asistenciaulima.utils.dialogs.FeedbackDialog
import kotlinx.android.synthetic.main.item_asesorias_activity.view.*
import org.jetbrains.anko.AnkoContext


class AsesoriasActivityRecyclerAdapter(private val AsesoriasList: MutableList<AsesoriaList>, private val callBackLongPress : (AsesoriaList) -> Unit) : RecyclerView.Adapter<AsesoriasActivityRecyclerAdapter.ViewHolder>() {

    override fun getItemCount():Int=AsesoriasList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_asesorias_activity, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            val asesoria = AsesoriasList[position]
            tvDiaIAA.text = asesoria.dia
            tvHoraIAA.text = asesoria.hora
            tvLugarIAA.text = asesoria.lugar
            layoutIAA.setOnLongClickListener {
                callBackLongPress(asesoria)
            true
            }
        }

    }



    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}