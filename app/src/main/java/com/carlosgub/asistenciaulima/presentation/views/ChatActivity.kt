package com.carlosgub.asistenciaulima.presentation.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.adapters.ChatActivityRecyclerAdapter
import com.carlosgub.asistenciaulima.presentation.presenters.ChatActivityPresenter
import com.carlosgub.asistenciaulima.utils.glide.GlideApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.toast
import javax.inject.Inject


class ChatActivity : BaseActivity(),ChatActivityPresenter.View {

    @Inject lateinit var mPresenter:ChatActivityPresenter

    lateinit var chat:Chat
    private lateinit var list: MutableList<String>
    private lateinit var mLayoutManager:StaggeredGridLayoutManager
    private lateinit var adapter: ChatActivityRecyclerAdapter
    private var info:Boolean=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setupWindowAnimations()
        setSupportActionBar(toolbarAC)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        val intent = intent
        chat = intent.getSerializableExtra("chat") as Chat
        val type = PreferenceManager.getDefaultSharedPreferences(applicationContext).getString("userType", "")
        if(type=="Alumno"){
            GlideApp.with(this)
                    .load(chat.imageUrl)
                    .error(R.drawable.avatar)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .apply(RequestOptions.circleCropTransform())
                    .into(avatarAC)
            nameTeacherAC.text = chat.nameProfesor
            info=true
        }else{
            nameTeacherAC.text =chat.nameAlumno
        }

        mPresenter.getNewChat(chat.id!!)

        list= mutableListOf()

        tvChatAC.setOnClickListener{
            val imm =  getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(tvChatAC,InputMethodManager.SHOW_IMPLICIT)
            mLayoutManager.smoothScrollToPosition(rvChatAC, null, list.size)
        }
        tvChatAC.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            if (b) {
                tvChatAC.performClick()
            }
        }


        ibSendAC.setOnClickListener {
            if(tvChatAC.text.isNotBlank() && tvChatAC.text.isNotEmpty()){
                mPresenter.sentMessage(chat.id!!, "${FirebaseAuth.getInstance().uid}-${tvChatAC.text}" )
                tvChatAC.setText("")
            }else{
                toast("Ingrese el mensaje a enviar")
            }
        }

        //Adapter
        adapter = ChatActivityRecyclerAdapter(list)
        mLayoutManager = StaggeredGridLayoutManager(1, 1)
        rvChatAC.layoutManager = mLayoutManager
        rvChatAC.visibility= View.VISIBLE
        rvChatAC.adapter = adapter
    }

    override val layout: Int get() = R.layout.activity_chat
    override val presenter: BasePresenter? get() = mPresenter

    override fun setNameTextView(name: String) {
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showMessage(title: String, message: String) {
    }

    override fun addChat(mensaje: String?) {
        if(mensaje!=null){
            mLayoutManager.smoothScrollToPosition(rvChatAC, null, list.size)
            list.add(mensaje)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.info-> {
                val intent = Intent(applicationContext,InformacionActivity::class.java)
                intent.putExtra("chat",chat )
                startActivity(intent)
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return if(info){
            val inflater = menuInflater
            inflater.inflate(R.menu.menu_toolbar_chat, menu)
            true
        }else{
            super.onCreateOptionsMenu(menu)
        }
    }

    /*private fun setupWindowAnimations() {
        val slide = Slide(Gravity.AXIS_PULL_BEFORE)
        slide.duration = 1000
        window.exitTransition = slide
    }*/

}
