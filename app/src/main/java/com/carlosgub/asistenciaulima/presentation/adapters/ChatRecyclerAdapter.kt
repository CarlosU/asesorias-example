package com.carlosgub.asistenciaulima.presentation.adapters

import android.preference.PreferenceManager
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.bumptech.glide.Priority
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.utils.glide.GlideApp
import kotlinx.android.synthetic.main.item_chat.view.*
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions




class ChatRecyclerAdapter(private val chatList: List<Chat>,private val activity:FragmentActivity,private var chatListFiltered:List<Chat>, private var callback:(Boolean) -> Unit,private var callbackClick: (View,Chat,String) ->Unit,private var callbackOnLongPress: (View,Chat,String) -> Unit) : RecyclerView.Adapter<ChatRecyclerAdapter.ViewHolder>(),Filterable {

    override fun getItemCount():Int=chatListFiltered.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            if(!chatListFiltered.isEmpty()){
                callback(true)
                val chat = chatListFiltered[position]
                val type = PreferenceManager.getDefaultSharedPreferences(context).getString("userType", "")
                if(type=="Alumno"){
                    teacherNameIC.text=chat.nameProfesor
                    GlideApp.with(context)
                            .load(chat.imageUrl)
                            .error(R.drawable.avatar)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .priority(Priority.HIGH)
                            .apply(RequestOptions.circleCropTransform())
                            .into(avatarIC)
                }else{
                    teacherNameIC.text=chat.nameAlumno
                }

                var chatText:String
                chatText = if(chat.chat!!.size>0){
                    chat.chat[chat.chat.size-1].replace("${chat.alumno}-","")
                }else{
                    ""
                }
                chatText = chatText.replace("${chat.profesor}-","")
                lastMessageIC.text = chatText

                cardViewIC.setOnClickListener {callbackClick(it,chat,type) }

                cardViewIC.setOnLongClickListener {
                    callbackOnLongPress(it,chat,type)
                true
                }
            }else{
                callback(false)
            }
        }
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    chatListFiltered = chatList
                } else {
                    val filteredList = mutableListOf<Chat>()
                    for (row in chatList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.nameProfesor.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }

                    chatListFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = chatListFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                chatListFiltered = filterResults.values as List<Chat>
                if(chatListFiltered.isEmpty()) callback(false)
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}