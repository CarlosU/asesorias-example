package com.carlosgub.asistenciaulima.presentation.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Documento
import kotlinx.android.synthetic.main.item_informacion.view.*


class DetalleActivityRecyclerAdapter(private val documentoList: MutableList<Documento>, private val callOnClickPress : (Documento) -> Unit, private val callBackLongPress : (Documento) -> Unit) : RecyclerView.Adapter<DetalleActivityRecyclerAdapter.ViewHolder>() {

    override fun getItemCount():Int=documentoList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_informacion, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            val documento = documentoList[position]
            tvNameII.text = documento.name_archivo
            tvSizeII.text = "( ${documento.size_archivo} )"
            if(documento.tipo_archivo=="jpg" || documento.tipo_archivo =="jpeg"){
                ivTypeII.setImageResource(R.mipmap.ic_jpg)
            }else if(documento.tipo_archivo=="png"){
                ivTypeII.setImageResource(R.mipmap.ic_png)
            }else if(documento.tipo_archivo=="txt"){
                ivTypeII.setImageResource(R.mipmap.ic_txt)
            }else if(documento.tipo_archivo=="pdf"){
                ivTypeII.setImageResource(R.mipmap.ic_pdf)
            }else if(documento.tipo_archivo=="doc" || documento.tipo_archivo =="docx"){
                ivTypeII.setImageResource(R.mipmap.ic_doc)
            }else if(documento.tipo_archivo=="xls" || documento.tipo_archivo =="xlsx"){
                ivTypeII.setImageResource(R.mipmap.ic_xls)
            }else if(documento.tipo_archivo=="ppt" || documento.tipo_archivo =="pptx"){
                ivTypeII.setImageResource(R.mipmap.ic_ppt)
            }
            layoutII.setOnClickListener{
                callOnClickPress(documento)
            }

            layoutII.setOnLongClickListener {
                callBackLongPress(documento)
            true
            }
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}