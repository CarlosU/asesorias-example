package com.carlosgub.asistenciaulima.presentation.views


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.StaggeredGridLayoutManager
import android.transition.Fade
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.presentation.BaseFragment
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.adapters.AsesoriasRecyclerAdapter
import com.carlosgub.asistenciaulima.presentation.presenters.AsesoriasPresenter
import kotlinx.android.synthetic.main.fragment_asesoria.*
import kotlinx.android.synthetic.main.item_asesorias.view.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 *
 */
class AsesoriasFragment : BaseFragment(), AsesoriasPresenter.View {

    @Inject
    lateinit var mPresenter: AsesoriasPresenter

    private lateinit var adapter:AsesoriasRecyclerAdapter
    private lateinit var inflaterFragment:MenuInflater
    private lateinit var menuFragment:Menu

    private lateinit var searchView:SearchView
    private lateinit var listChat: List<PeticionAsesoria>

    private var selectedItems: MutableList<PeticionAsesoria> = mutableListOf()


    private lateinit var mContext:Context

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        errorFA.visibility=View.GONE

        pbFA.indeterminateDrawable.setColorFilter(resources.getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN)

        mPresenter.setContext(mContext)
        mPresenter.getAsesorias()

        setHasOptionsMenu(true)
        //mPresenter.getHistory()

    }

    override val layout: Int get() = R.layout.fragment_asesoria

    override val presenter: BasePresenter? get() = mPresenter

    override fun showProgress() {
        pbFA.visibility = View.VISIBLE
    }

    override fun hideProgress() {
    }

    override fun showMessage(title: String, message: String) {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.delete->{
                alert("Estas apunto de cancelar la/las siguiente/s solicitud/es de asesoria/s", "Eliminar solicitud de Asesoria") {
                    positiveButton("Si") {
                        mPresenter.eliminarAsesorias(selectedItems)
                        startActivity(Intent(mContext,CargandoActivity::class.java))
                    }
                    negativeButton("No") {}
                }.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext=context

    }

    override fun mostrarAsesorias(list: List<PeticionAsesoria>) {
        pbFA.visibility = View.GONE
        listChat = list
        if(!list.isEmpty()){
            adapter = AsesoriasRecyclerAdapter(list,activity!!,list,{
                if(errorFA.visibility == View.GONE){
                    if(!it){
                        noAsesoriasFA.visibility = View.VISIBLE
                    }else{
                        noAsesoriasFA.visibility = View.GONE
                    }
                }
            },{view,asesoria,type->
                if(!selectedItems.isEmpty()){
                    onItemLongClick(view, asesoria,type)
                }else{
                    var intent = Intent(mContext,DetalleActivity::class.java)
                    intent.putExtra("peticionAsesoria",asesoria)
                    startActivity(intent)
                }
            },{view,chat,type-> onItemLongClick(view,chat,type) })
            rvAsesoriasFA.visibility=View.VISIBLE
            errorFA.visibility=View.GONE
            rvAsesoriasFA.layoutManager = StaggeredGridLayoutManager(1, 1)
            rvAsesoriasFA.adapter = adapter
            adapter.notifyDataSetChanged()
        }else{
            errorFA.visibility=View.VISIBLE
            rvAsesoriasFA.visibility=View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu,inflater:MenuInflater) {
        inflaterFragment = inflater
        menuFragment = menu
        // Associate searchable configuration with the SearchView
        searchView = menu.findItem(R.id.search).actionView as SearchView

        searchView.queryHint = "Busca..."

        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if(listChat.isNotEmpty())adapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                if(listChat.isNotEmpty())adapter.filter.filter(query)
                return false
            }
        })
    }

    private fun onItemLongClick(view: View, peticionAsesoria: PeticionAsesoria,type:String): Boolean {
        if(type=="Alumno"){
            if(peticionAsesoria.estadoNumber == 1 ){
                if (!selectedItems.contains(peticionAsesoria)) {
                    view.layoutIA.setBackgroundColor(resources.getColor(R.color.grey_200))
                    selectedItems.add(peticionAsesoria)
                } else {
                    view.layoutIA.setBackgroundColor(resources.getColor(R.color.white))
                    selectedItems.remove(peticionAsesoria)
                }
                if (!selectedItems.isEmpty()) {
                    menuFragment.clear()
                    inflaterFragment.inflate(R.menu.menu_toolbar_chat_edit, menuFragment)
                } else {
                    menuFragment.clear()
                    inflaterFragment.inflate(R.menu.menu_toolbar_main, menuFragment)
                }
            }else{
                toast("Solo puede eliminar asesorias pendientes por aprobar.")
            }
        }else{
            toast("Usted solo puede visualizar las asesorias.")
        }

        return true
    }

    override fun hideCargando() {
        CargandoActivity.fa.finish()
    }

    override fun mostrarMenu() {
        menuFragment.clear()
        inflaterFragment.inflate(R.menu.menu_toolbar_main, menuFragment)
    }

}
