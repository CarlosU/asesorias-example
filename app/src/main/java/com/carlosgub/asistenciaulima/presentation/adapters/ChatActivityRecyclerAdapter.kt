package com.carlosgub.asistenciaulima.presentation.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.carlosgub.asistenciaulima.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.item_chat_activity.view.*

class ChatActivityRecyclerAdapter(private val chatList: MutableList<String>) : RecyclerView.Adapter<ChatActivityRecyclerAdapter.ViewHolder>() {

    override fun getItemCount():Int=chatList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_chat_activity, parent, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder.itemView) {
            val mensaje = chatList[position]
            val contenido:MutableList<String> = mensaje.split("-") as MutableList<String>
            if(contenido[0]== FirebaseAuth.getInstance().currentUser?.uid){

                lyEmisorICA.visibility=View.VISIBLE
                contenido.removeAt(0)
                tvEmisorICA.text=""
                for(i in contenido){
                    tvEmisorICA.text = "${tvEmisorICA.text} $i"
                }
            }else{
                lyReceptorICA.visibility=View.VISIBLE
                contenido.removeAt(0)
                tvReceptorICA.text=""
                for(i in contenido){
                    tvReceptorICA.text = "${tvReceptorICA.text} $i"
                }
            }
        }
    }



    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

}