package com.carlosgub.asistenciaulima.presentation.presenters

import android.support.v4.app.Fragment
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.domain.usecases.GetChatListUseCase
import com.carlosgub.asistenciaulima.domain.usecases.GetUserUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.views.AsesoriasFragment
import com.carlosgub.asistenciaulima.presentation.views.ChatFragment
import com.google.firebase.auth.FirebaseAuth
import rx.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class MainPresenter @Inject constructor(var view: View,
                                        private val useCase: GetChatListUseCase,
                                        private val usecase2:GetUserUseCase): BasePresenter {


    var idSelected = -1

    fun prepareToShow(itemId: Int): Boolean {
        if (idSelected != itemId) {
            when (itemId) {
                R.id.chat -> {
                    view.showFragmentView(ChatFragment())
                }
                R.id.asesorias -> {
                    view.showFragmentView(AsesoriasFragment())
                }
            }
            idSelected = itemId
        }
        return true
    }

    fun createChat(idProfesor:String){
        view.showProgress()
        usecase2.getUser(idProfesor)
                .subscribeOn(Schedulers.io())
                .subscribe {profesor->
                    usecase2.getUser(FirebaseAuth.getInstance().uid!!)
                            .subscribeOn(Schedulers.io())
                            .subscribe { yop->
                                if(profesor.exist && profesor.type == "Profesor"){
                                    useCase.getChatsOneTime("Alumno")
                                            .subscribeOn(Schedulers.io())
                                            .subscribe {chats->
                                                var duplicado = false
                                                chats.forEach { chat->
                                                    if(chat.profesor==profesor.id) duplicado=true
                                                }
                                                if(!duplicado){
                                                    useCase.createChat(Chat(yop.id,idProfesor, arrayListOf(), Date().time,yop.name,profesor.name,null,profesor.imageUrl)){
                                                        if(it){
                                                            view.hideProgress()
                                                            view.showMessage("Se agrego")
                                                        }else{
                                                            view.hideProgress()
                                                            view.showMessage("Error")
                                                        }
                                                    }
                                                }else{
                                                    view.hideProgress()
                                                    view.showMessage("El profesor ya lo tienes agregado")
                                                }
                                            }
                                }else{
                                    view.hideProgress()
                                    view.showMessage("No existe el profesor")
                                }

                            }
                }
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
    }

    interface View {
        fun showFragmentView(fragment: Fragment)
        fun showProgress()
        fun hideProgress()
        fun showMessage(msg:String)
    }

}



