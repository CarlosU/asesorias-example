package com.carlosgub.asistenciaulima.presentation.views


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.util.Pair
import android.support.v7.widget.StaggeredGridLayoutManager
import android.transition.Fade
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.presentation.BaseFragment
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.adapters.ChatRecyclerAdapter
import com.carlosgub.asistenciaulima.presentation.presenters.ChatPresenter
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.item_chat.view.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 *
 */
class ChatFragment : BaseFragment(), ChatPresenter.View {

    @Inject lateinit var mPresenter: ChatPresenter
    private lateinit var mContext:Context
    private lateinit var adapter:ChatRecyclerAdapter
    private lateinit var inflaterFragment:MenuInflater
    private lateinit var menuFragment:Menu

    private lateinit var searchView:SearchView
    private lateinit var listChat: List<Chat>

    private var selectedItems: MutableList<Chat> = mutableListOf()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        errorFC.visibility=View.GONE

        pbFC.indeterminateDrawable.setColorFilter(resources.getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN)

        mPresenter.setContext(mContext)
        mPresenter.getChats()

        setHasOptionsMenu(true)

    }

    override val layout: Int get() = R.layout.fragment_chat

    override val presenter: BasePresenter? get() = mPresenter



    override fun showProgress() {
        pbFC.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        pbFC.visibility = View.GONE
    }

    override fun showMessage(title: String, message: String) {
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.delete->{
                alert("Estas apunto de eliminar el/los chat/s", "Eliminar chat") {
                    positiveButton("Si") {
                        mPresenter.eliminarChats(selectedItems)
                        startActivity(Intent(mContext,CargandoActivity::class.java))
                    }
                    negativeButton("No") {}
                }.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun limpiarCampo(){

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext=context
    }

    override fun mostrarChat(list: List<Chat>) {
        pbFC.visibility = View.GONE
        listChat = list
        if(!list.isEmpty()){
            adapter = ChatRecyclerAdapter(list,activity!!,list,{
                if(errorFC.visibility == View.GONE){
                    if(!it){
                        noChatFC.visibility = View.VISIBLE
                    }else{
                        noChatFC.visibility = View.GONE
                    }
                }
            },{view,chat,type->
                if(!selectedItems.isEmpty()){
                    onItemLongClick(view, chat,type)
                }else{
                    val intent= Intent(view.context,ChatActivity::class.java)
                    intent.putExtra("chat",chat )
                    activity!!.window.enterTransition = Fade(Fade.IN)
                    activity!!.window.enterTransition = Fade(Fade.OUT)
                    val pairImageView = Pair.create<View,String>(view.avatarIC, view.context.getString(R.string.transition_avatar))
                    val pairNameText = Pair.create<View,String>(view.teacherNameIC, view.context.getString(R.string.transition_name))
                    val pairs = ArrayList<Pair<View, String>>()
                    pairs.add(pairImageView)
                    pairs.add(pairNameText)

                    val pairArray:Array<Pair<View, String>> = pairs.toTypedArray()

                    val options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(activity!!,*pairArray)
                    view.context.startActivity(intent,options.toBundle())
                }
            },{view,chat,type-> onItemLongClick(view,chat,type) })
            rvFC.visibility=View.VISIBLE
            errorFC.visibility=View.GONE
            rvFC.layoutManager = StaggeredGridLayoutManager(1, 1)
            rvFC.adapter = adapter
            adapter.notifyDataSetChanged()
        }else{
            errorFC.visibility=View.VISIBLE
            rvFC.visibility=View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu,inflater:MenuInflater) {
        inflaterFragment = inflater
        menuFragment = menu
        // Associate searchable configuration with the SearchView
        searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.queryHint = "Busca..."

        // listening to search query text change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if(listChat.isNotEmpty())adapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                if(listChat.isNotEmpty())adapter.filter.filter(query)
                return false
            }
        })
    }

    private fun onItemLongClick(view: View, agendaItem: Chat,type:String): Boolean {
        if(type=="Alumno") {
            if (!selectedItems.contains(agendaItem)) {
                view.layoutIC.setBackgroundColor(resources.getColor(R.color.grey_200))
                selectedItems.add(agendaItem)
            } else {
                view.layoutIC.setBackgroundColor(resources.getColor(R.color.white))
                selectedItems.remove(agendaItem)
            }
            if (!selectedItems.isEmpty()) {
                menuFragment.clear()
                inflaterFragment.inflate(R.menu.menu_toolbar_chat_edit, menuFragment)
            } else {
                menuFragment.clear()
                inflaterFragment.inflate(R.menu.menu_toolbar_main, menuFragment)
            }
        }else{
            toast("Usted no puede eliminar chats.")
        }
        return true
    }

    override fun hideCargando() {
        CargandoActivity.fa.finish()
    }

    override fun mostrarMenu() {
        menuFragment.clear()
        inflaterFragment.inflate(R.menu.menu_toolbar_main, menuFragment)
    }
}
