package com.carlosgub.asistenciaulima.presentation


import com.carlosgub.asistenciaulima.presentation.presenters.*
import com.carlosgub.asistenciaulima.presentation.views.*
import dagger.Binds
import dagger.Module


/**
 * @author Carlosgub
 * @link github.com/carlosgub
 */
@Module
abstract class BindingModule {

    @Binds abstract fun provideLoginView(activity: LoginActivity): LoginPresenter.View

    @Binds abstract fun provideMainView(activity: MainActivity): MainPresenter.View

    @Binds abstract fun provideChatActivityView(activity: ChatActivity): ChatActivityPresenter.View

    @Binds abstract fun provideChatView(fragment: ChatFragment): ChatPresenter.View

    @Binds abstract fun provideAesoriaView(fragment: AsesoriasFragment): AsesoriasPresenter.View

    @Binds abstract fun provideAesoriasActivityView(activity: AsesoriasActivity): AsesoriasActivityPresenter.View

    @Binds abstract fun provideDetalleView(activity: DetalleActivity): DetallePresenter.View

}