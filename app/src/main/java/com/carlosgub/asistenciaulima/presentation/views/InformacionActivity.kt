package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import kotlinx.android.synthetic.main.activity_informacion.*

class InformacionActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setupWindowAnimations()
        setSupportActionBar(toolbarAI)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val intent = intent
        val chat = intent.getSerializableExtra("chat") as Chat

        llQRAI.setOnClickListener {
            val intentQr = Intent(this,QrActivity::class.java)
            intentQr.putExtra("qr",chat.profesor)
            startActivity(intentQr)
        }

        llAsesoriasAI.setOnClickListener {
            val intentAsesorias = Intent(this,AsesoriasActivity::class.java)
            intentAsesorias.putExtra("chat",chat )
            startActivity(intentAsesorias)
        }
    }

    override val layout: Int get() = R.layout.activity_informacion
    override val presenter: BasePresenter? get() = null

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /*private fun setupWindowAnimations() {
        val slide = Slide(Gravity.LEFT)
        slide.duration = 1000
        window.enterTransition = slide
    }*/
}
