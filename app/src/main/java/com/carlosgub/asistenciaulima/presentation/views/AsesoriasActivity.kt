package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.MenuItem
import android.view.View
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.domain.model.AsesoriaList
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.adapters.AsesoriasActivityRecyclerAdapter
import com.carlosgub.asistenciaulima.presentation.presenters.AsesoriasActivityPresenter
import com.carlosgub.asistenciaulima.utils.dialogs.FeedbackDialog
import kotlinx.android.synthetic.main.activity_asesorias.*
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.contentView
import org.jetbrains.anko.toast
import javax.inject.Inject

class AsesoriasActivity : BaseActivity(),AsesoriasActivityPresenter.View {

    @Inject
    lateinit var mPresenter: AsesoriasActivityPresenter

    private lateinit var chat:Chat


    private lateinit var adapter: AsesoriasActivityRecyclerAdapter

    private lateinit var mLayoutManager:StaggeredGridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setSupportActionBar(toolbarAA)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val intent = intent
        chat = intent.getSerializableExtra("chat") as Chat
        mPresenter.getAsesorias(chat.profesor)
        startActivity(Intent(this,CargandoActivity::class.java))
    }

    override val layout: Int get() = R.layout.activity_asesorias

    override val presenter: BasePresenter? get() = mPresenter


    override fun showAsesorias(list: MutableList<AsesoriaList>) {
        CargandoActivity.fa.finish()
        //Adapter
        adapter = AsesoriasActivityRecyclerAdapter(list){asesoria->
            val feedbackDialogUi by lazy {
                contentView?.let {
                    FeedbackDialog(AnkoContext.create(this, it))
                }
            }

            feedbackDialogUi?.okButton?.setOnClickListener {
                startActivity(Intent(this,CargandoActivity::class.java))
                val tema = feedbackDialogUi!!.feedbackText.text.toString()
                mPresenter.enviarPeticionAsesoria(PeticionAsesoria(
                        dia=asesoria.dia,
                        hora=asesoria.hora,
                        lugar = asesoria.lugar,
                        alumno = chat.alumno,
                        profesor = chat.profesor,
                        nameAlumno = chat.nameAlumno,
                        nameProfesor = chat.nameProfesor,
                        tema = tema,
                        estado = "ENVIADO",
                        horaEpoch = asesoria.horaEpoch,
                        imageUrl = chat.imageUrl,
                        documentos = mutableListOf()))
                feedbackDialogUi!!.dialog.dismiss()
            }

            feedbackDialogUi?.cancelButton?.setOnClickListener {
                feedbackDialogUi!!.dialog.dismiss()
            }
        }
        mLayoutManager = StaggeredGridLayoutManager(1, 1)
        rvAA.layoutManager = mLayoutManager
        rvAA.visibility= View.VISIBLE
        rvAA.adapter = adapter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showMessage(title: String, message: String) {
    }

    override fun hideCargando() {
        CargandoActivity.fa.finish()
    }

    override fun mostrarMensaje(mensaje: String) {
        toast(mensaje)
    }
}
