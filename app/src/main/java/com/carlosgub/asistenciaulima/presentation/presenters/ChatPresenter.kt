package com.carlosgub.asistenciaulima.presentation.presenters

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.domain.usecases.GetChatListUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class ChatPresenter  @Inject constructor(var view: View,
                                         private val useCase: GetChatListUseCase): BasePresenter {

    private var cs:CompositeSubscription = CompositeSubscription()
    private lateinit var mContext: Context
    private var contador = 0

    fun setContext(context: Context){
        mContext=context
    }

    fun getChats(){
        view.showProgress()
        val type = PreferenceManager.getDefaultSharedPreferences(mContext).getString("userType", "")
        view.showProgress()
        cs.add( useCase.getChats(type)
            .subscribe({
                view.hideProgress()
                view.mostrarChat(it)
            },{
            }))
    }

    fun eliminarChats(chatList:MutableList<Chat>){
        useCase.deleteChats(chatList){
            contador++
            if(contador==chatList.size){
                view.hideCargando()
                contador=0
                view.mostrarMenu()
            }
        }
    }

    private fun clearSubscriptions(){
        cs.clear()
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        clearSubscriptions()
    }

    interface View: BaseView {
        fun limpiarCampo()
        fun mostrarChat(list:List<Chat>)
        fun hideCargando()
        fun mostrarMenu()
    }

}