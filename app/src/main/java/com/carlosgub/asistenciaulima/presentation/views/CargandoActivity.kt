package com.carlosgub.asistenciaulima.presentation.views

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.carlosgub.asistenciaulima.R
import kotlinx.android.synthetic.main.activity_cargando.*

class CargandoActivity : AppCompatActivity() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var fa:Activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cargando)
        this.setFinishOnTouchOutside(false)
        pbAC.indeterminateDrawable.setColorFilter(resources.getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN)
        fa = this
    }

    override fun onBackPressed() {
    }


}
