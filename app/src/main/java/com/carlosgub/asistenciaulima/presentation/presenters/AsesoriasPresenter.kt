package com.carlosgub.asistenciaulima.presentation.presenters

import android.content.Context
import android.preference.PreferenceManager
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.usecases.GetAsesoriaUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

class AsesoriasPresenter @Inject constructor(var view: View,
                                             private val useCase: GetAsesoriaUseCase): BasePresenter {

    private lateinit var mContext:Context
    private var cs: CompositeSubscription = CompositeSubscription()
    private var contador = 0

    fun setContext(context:Context){
        mContext = context
    }

    fun getAsesorias(){
        view.showProgress()
        val type = PreferenceManager.getDefaultSharedPreferences(mContext).getString("userType", "")
        view.showProgress()
        cs.add(useCase.getPeticionesAsesoria(type)
                .map {list->
                    var listPeticion = mutableListOf<PeticionAsesoria>()
                    listPeticion.addAll(list)
                    listPeticion.sortedWith(compareBy( PeticionAsesoria::estadoNumber,PeticionAsesoria::horaEpoch)).toMutableList()
                }

                .subscribe({
                    view.hideProgress()
                    view.mostrarAsesorias(it)
                },{
                }))
    }

    fun eliminarAsesorias(chatList:MutableList<PeticionAsesoria>){
        useCase.deleteAsesorias(chatList){
            contador++
            if(contador==chatList.size){
                view.hideCargando()
                contador = 0
                view.mostrarMenu()
            }
        }
    }
    private fun clearSubscriptions(){
        cs.clear()
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        clearSubscriptions()
    }

    interface View: BaseView{
        fun mostrarAsesorias(list: List<PeticionAsesoria>)
        fun hideCargando()
        fun mostrarMenu()
    }
}