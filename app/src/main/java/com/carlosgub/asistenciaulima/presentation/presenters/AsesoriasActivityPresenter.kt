package com.carlosgub.asistenciaulima.presentation.presenters

import com.carlosgub.asistenciaulima.domain.model.AsesoriaList
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.usecases.GetAsesoriaUseCase
import com.carlosgub.asistenciaulima.domain.usecases.GetDateUseCase
import com.carlosgub.asistenciaulima.domain.usecases.GetPeticionUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.*
import javax.inject.Inject

class AsesoriasActivityPresenter  @Inject constructor(var view: View,
                                                      private val useCase: GetAsesoriaUseCase,
                                                      private val useCase2: GetDateUseCase,
                                                      private val useCase3: GetPeticionUseCase): BasePresenter {

    var cs:CompositeSubscription = CompositeSubscription()

        fun getAsesorias(uid:String){
        useCase2.getDates{ listDates->
            cs.add(useCase.getAsesorias(uid)
                    .subscribeOn(Schedulers.io())
                    .subscribe {listAsesorias->
                        val list:MutableList<AsesoriaList> = mutableListOf()
                        val c1 = Calendar.getInstance()
                        c1.time = Date(listDates[0])
                        if(Date().time>c1.time.time) c1.time = Date(Date().time)
                        c1.set(Calendar.HOUR_OF_DAY, 0)
                        c1.set(Calendar.MINUTE, 0)
                        c1.set(Calendar.SECOND, 0)
                        c1.set(Calendar.MILLISECOND, 0)
                        val c2 = Calendar.getInstance()
                        c2.time = Date(listDates[1])

                        while (c1.before(c2)) {
                            listAsesorias.forEach { asesoria->
                                if(c1.get(Calendar.DAY_OF_WEEK) == asesoria.dayOfWeek){
                                    var horas = asesoria.horaInicio.split(" ")
                                    var x:Long = if(horas[1] == "a.m"){
                                        horas[0].toLong()* 1000 * 60 * 60
                                    }else{
                                        (12 + horas[0].toLong()) * 1000 * 60 * 60
                                    }
                                    list.add(AsesoriaList(
                                            dia ="${c1.get(Calendar.DAY_OF_MONTH)}/${c1.get(Calendar.MONTH)+1}/${c1.get(Calendar.YEAR)} (${asesoria.dia})",
                                            hora = "${asesoria.horaInicio} - ${asesoria.horaFin}",
                                            lugar = asesoria.lugar,
                                            horaEpoch = c1.timeInMillis+x ))
                                }
                            }
                            c1.add(Calendar.DATE, 1)
                        }
                        view.showAsesorias(list)
                    }
            )
        }

    }

    fun enviarPeticionAsesoria(peticion:PeticionAsesoria){
        useCase3.obtenerAsesorias()
                .subscribe {
                    var estado = false
                    it.forEach { peticionUnit->
                        if(peticionUnit.profesor==peticion.profesor && peticionUnit.horaEpoch == peticion.horaEpoch){
                            estado=true
                            return@forEach
                        }
                    }
                    if(!estado){
                        useCase3.enviarPeticionAsesoria(peticion){
                            view.hideCargando()
                        }
                    }else{
                        view.hideCargando()
                        view.mostrarMensaje("No puedes enviar otra asesoria para el mismo dia")
                    }
                }
    }

    private fun clearSubscriptions(){
        cs.clear()
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        clearSubscriptions()
    }

    interface View: BaseView {
        fun showAsesorias(list:MutableList<AsesoriaList>)
        fun hideCargando()
        fun mostrarMensaje(mensaje:String)
    }

}