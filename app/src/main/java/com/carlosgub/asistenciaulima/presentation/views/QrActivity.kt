package com.carlosgub.asistenciaulima.presentation.views

import android.graphics.Bitmap
import android.os.Bundle
import com.bumptech.glide.Glide
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import kotlinx.android.synthetic.main.activity_qr.*
import java.util.HashMap

class QrActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = intent
        val qr = intent.getStringExtra("qr")
        textToImage(qr,500,500)
    }

    @Throws(WriterException::class, NullPointerException::class)
    fun textToImage(text: String, width: Int, height: Int) {

        val bitMatrix = MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE,
                width, height, null)


        val bitMatrixWidth = bitMatrix.width
        val bitMatrixHeight = bitMatrix.height
        val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)

        val colorWhite = -0x1
        val colorBlack = -0x1000000

        for (y in 0 until bitMatrixHeight) {
            val offset = y * bitMatrixWidth
            for (x in 0 until bitMatrixWidth) {
                pixels[offset + x] = if (bitMatrix.get(x, y)) colorBlack else colorWhite
            }
        }
        val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)

        bitmap.setPixels(pixels, 0, width, 0, 0, bitMatrixWidth, bitMatrixHeight)

        Glide.with(applicationContext)
                .load(bitmap)
                .into(ivQRAQ)
    }
    override val layout: Int get() = R.layout.activity_qr

    override val presenter: BasePresenter? get() = null
}
