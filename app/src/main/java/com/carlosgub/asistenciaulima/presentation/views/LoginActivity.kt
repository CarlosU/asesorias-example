package com.carlosgub.asistenciaulima.presentation.views

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.annotation.ColorInt
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.presentation.BaseActivity
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.presenters.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class LoginActivity : BaseActivity() , LoginPresenter.View{


    @Inject lateinit var mPresenter:LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPresenter.context(applicationContext)
        pbAL.visibility = View.GONE

        pbAL.indeterminateDrawable.setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN)

        setCursorPointerColor(emailAL,resources.getColor(R.color.colorPrimary))
        setCursorPointerColor(passwordAL,resources.getColor(R.color.colorPrimary))

        btLoginAL.setOnClickListener{
            btLoginAL.isEnabled = false
            mPresenter.login(emailAL.text.toString(),passwordAL.text.toString())
        }

    }

    override val layout: Int get() = R.layout.activity_login

    override val presenter: BasePresenter? get() = mPresenter

    override fun showProgress() {
        btLoginAL.isEnabled = false
        pbAL.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        btLoginAL.isEnabled = true
        pbAL.visibility = View.GONE
    }

    override fun showMessage(title: String, message: String) {
    }

    override fun loginCorrecto() {
        pbAL.indeterminateDrawable.setColorFilter(resources.getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN)
        toast("login")
        val intent = Intent(this,MainActivity::class.java)
        /*window.enterTransition = Fade(Fade.IN)
        window.enterTransition = Fade(Fade.OUT)
        val pairPb = Pair.create<View,String>(pb,
                applicationContext.getString(R.string.transition_pb))
        val pairs = ArrayList<Pair<View, String>>()
        pairs.add(pairPb)
        val pairArray:Array<Pair<View,String>> = pairs.toTypedArray()

        val options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this,*pairArray)*/
        startActivity(intent)
        finish()
    }

    override fun loginIncorrecto(msg:String) {
        toast(msg)
    }

    private fun setCursorPointerColor(view: EditText, @ColorInt color: Int) {
        try {
            //get the pointer resource id
            var field = TextView::class.java.getDeclaredField("mTextSelectHandleRes")
            field.isAccessible = true
            val drawableResId = field.getInt(view)

            //get the editor
            field = TextView::class.java.getDeclaredField("mEditor")
            field.isAccessible = true
            val editor = field.get(view)

            //tint drawable
            val drawable = ContextCompat.getDrawable(view.context, drawableResId)!!
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN)

            //set the drawable
            field = editor.javaClass.getDeclaredField("mSelectHandleCenter")
            field.isAccessible = true
            field.set(editor, drawable)

        } catch (ex: Exception) {
        }
    }

    override fun onBackPressed() {
    }
}
