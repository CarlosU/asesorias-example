package com.carlosgub.asistenciaulima.presentation

import java.io.IOException

interface BasePresenter {

    /**
     * This method will be executed on
     * [AppCompatActivity.onResume] in case presenter is attached to activity
     * [Fragment.onResume]  in case presenter is attached to fragment
     */
    fun onResume()

    /**
     * This method will be executed on
     * [AppCompatActivity.onPause] in case presenter is attached to activity
     * [Fragment.onPause]  in case presenter is attached to fragment
     */
    fun onPause()

    /**
     * This method will be executed on
     * [AppCompatActivity.onDestroy] in case presenter is detached to activity
     * [Fragment.onDestroy] in case presenter is detached to fragment
     */
    fun onDestroy()

    /**
     * Custom Message Handler
     */
    fun Throwable.message(): String {
        printStackTrace()
        when (this) {
            is IOException -> return "No se ha podido conectar con el servidor. Comprueba tu conexión a Internet y vuelve a intentarlo."
            else               -> return "Ha ocurrido un error"
        }
    }

}