package com.carlosgub.asistenciaulima.presentation

import com.carlosgub.asistenciaulima.presentation.views.*
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author Carlos Ugaz
 * @link github.com/carlosgub
 */
@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeSplashActivityInjector(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributeLoginActivityInjector(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeMainActivityInjector(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeChatActivityInjector(): ChatActivity

    @ContributesAndroidInjector
    abstract fun contributeChatFragmentInjector(): ChatFragment

    @ContributesAndroidInjector
    abstract fun contributeAsesoriaFragmentInjector(): AsesoriasFragment

    @ContributesAndroidInjector
    abstract fun contributeInformationActivityInjector(): InformacionActivity

    @ContributesAndroidInjector
    abstract fun contributeQrActivityInjector(): QrActivity

    @ContributesAndroidInjector
    abstract fun contributeAsesoriasActivityInjector(): AsesoriasActivity

    @ContributesAndroidInjector
    abstract fun contributeDetalleActivityInjector(): DetalleActivity


}