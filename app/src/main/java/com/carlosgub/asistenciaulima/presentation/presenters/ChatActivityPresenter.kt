package com.carlosgub.asistenciaulima.presentation.presenters

import com.carlosgub.asistenciaulima.domain.usecases.GetChatListUseCase
import com.carlosgub.asistenciaulima.presentation.BasePresenter
import com.carlosgub.asistenciaulima.presentation.BaseView
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject


class ChatActivityPresenter  @Inject constructor(var view: ChatActivityPresenter.View,
                                                 private val useCase: GetChatListUseCase): BasePresenter {
    private val cs = CompositeSubscription()

    fun getNewChat(idChat:String){
        cs.add(useCase.getNewChat(idChat)
                .subscribe {
                    view.addChat(it)
                })
    }

    fun sentMessage(idChat: String,message:String){
        useCase.sentMessage(idChat,message)
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onDestroy() {
        cs.clear()
    }

    interface View: BaseView {
        fun setNameTextView(name:String)
        fun addChat(mensaje:String?)
    }

}