package com.carlosgub.asistenciaulima

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * @author Carlos Ugaz
 * @link github.com/carlosgub
 */
@Module
class AppModule {

    /**
     * App Context
     */

    @Provides fun provideContext(app: App): Context = app

}
