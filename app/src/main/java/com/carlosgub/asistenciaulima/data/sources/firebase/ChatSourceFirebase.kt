package com.carlosgub.asistenciaulima.data.sources.firebase

import com.carlosgub.asistenciaulima.domain.model.Chat
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import com.kelvinapps.rxfirebase.RxFirebaseDatabase
import rx.Observable
import java.util.*
import javax.inject.Inject


class ChatSourceFirebase @Inject constructor(private val firebaseDatabase: FirebaseDatabase) {

    fun getChat(type:String) : Observable<DataSnapshot> {
        val ref = if(type=="Alumno"){
            firebaseDatabase.reference
                    .child("chat").orderByChild("alumno").equalTo(FirebaseAuth.getInstance().uid)
        }else{
            firebaseDatabase.reference
                    .child("chat").orderByChild("profesor").equalTo(FirebaseAuth.getInstance().uid)
        }
        return RxFirebaseDatabase.observeValueEvent(ref)
    }

    fun getChatOneTime(type:String) : Observable<DataSnapshot> {
        val ref = if(type=="Alumno"){
            firebaseDatabase.reference
                    .child("chat").orderByChild("alumno").equalTo(FirebaseAuth.getInstance().uid)
        }else{
            firebaseDatabase.reference
                    .child("chat").orderByChild("profesor").equalTo(FirebaseAuth.getInstance().uid)
        }
        return RxFirebaseDatabase.observeSingleValueEvent(ref)
    }

    fun getNewChat(idChat:String): Observable<RxFirebaseChildEvent<DataSnapshot>>{
        val ref = firebaseDatabase.reference
                .child("chat").child(idChat).child("chat")
        return RxFirebaseDatabase.observeChildEvent(ref)
    }

    fun sentMessage(idChat: String,message:String){
        val key = firebaseDatabase.reference
                .child("chat").child(idChat).child("chat").push().key!!
        firebaseDatabase.reference
                .child("chat").child(idChat).child("chat").child(key).setValue(message)
        firebaseDatabase.reference
                .child("chat").child(idChat).child("time").setValue(Date().time)
    }

    fun createChat(chat:Chat, callback: (Boolean) -> Unit){
        val key = firebaseDatabase.reference
                .child("chat").push().key!!
        firebaseDatabase.reference.child("chat").child(key).setValue(chat).addOnSuccessListener {
            callback(true)
        }.addOnFailureListener{
            callback(false)
        }
    }

    fun deleteChats(chatList:MutableList<Chat>, callback: (Boolean) -> Unit){
        chatList.forEach {
            firebaseDatabase.reference.child("chat").child(it.id!!).setValue(null).addOnSuccessListener {_->
                callback(true)
            }.addOnFailureListener{_->
                callback(false)
            }
        }
    }

}