package com.carlosgub.asistenciaulima.data.sources.firebase

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import com.kelvinapps.rxfirebase.RxFirebaseDatabase
import rx.Observable
import java.util.*
import javax.inject.Inject


class UserSourceFirebase @Inject constructor(private val firebaseDatabase: FirebaseDatabase) {

    fun getUserType(uid:String) : Observable<DataSnapshot> {
        val ref = firebaseDatabase.reference
                .child("user").child(uid).child("type")
        return RxFirebaseDatabase.observeSingleValueEvent(ref)
    }

    fun getUser(uid:String) : Observable<DataSnapshot> {
        val ref = firebaseDatabase.reference
                .child("user").child(uid)
        return RxFirebaseDatabase.observeSingleValueEvent(ref)
    }
}