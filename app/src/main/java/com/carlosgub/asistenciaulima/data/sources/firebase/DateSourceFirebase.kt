package com.carlosgub.asistenciaulima.data.sources.firebase

import com.carlosgub.asistenciaulima.R
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.kelvinapps.rxfirebase.BuildConfig
import javax.inject.Inject


class DateSourceFirebase @Inject constructor(private val firebaseDatabase: FirebaseDatabase) {

    val mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
    var cacheExpiration:Long = 43200

    fun getDates(callback:(MutableList<Long>) -> Unit){
        val listDates:MutableList<Long> = mutableListOf()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()

        mFirebaseRemoteConfig.setConfigSettings(configSettings)
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults)

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener {
            if(it.isSuccessful){
                listDates.add(mFirebaseRemoteConfig.getLong("dia_inicio_ciclo"))
                listDates.add(mFirebaseRemoteConfig.getLong("dia_fin_ciclo"))
                callback(listDates)
            }
        }

    }


}