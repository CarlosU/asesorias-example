package com.carlosgub.asistenciaulima.data.repository

import com.carlosgub.asistenciaulima.data.sources.firebase.DateSourceFirebase
import com.carlosgub.asistenciaulima.domain.repository.DateRepository
import javax.inject.Inject

class DateDataRepository @Inject constructor(private val source: DateSourceFirebase) : DateRepository {

    override fun getDates(callback:(MutableList<Long>) -> Unit){
        source.getDates {
            callback(it)
        }
    }
}