package com.carlosgub.asistenciaulima.data.sources.firebase

import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.kelvinapps.rxfirebase.RxFirebaseDatabase
import rx.Observable
import javax.inject.Inject


class AsesoriaSourceFirebase @Inject constructor(private val firebaseDatabase: FirebaseDatabase) {

    fun getAsesorias(uid:String) : Observable<DataSnapshot> {
        val ref = firebaseDatabase.reference
                .child("asesoria").child(uid)
        return RxFirebaseDatabase.observeValueEvent(ref)
    }

    fun deleteAsesorias(AsesoriasList:MutableList<PeticionAsesoria>, callback: (Boolean) -> Unit){
        AsesoriasList.forEach {
            firebaseDatabase.reference.child("solicitudes").child(it.id!!).setValue(null).addOnSuccessListener {_->
                callback(true)
            }.addOnFailureListener{_->
                callback(false)
            }
        }
    }

    fun getPeticion(type:String) : Observable<DataSnapshot> {
        val ref = if(type=="Alumno"){
            firebaseDatabase.reference
                    .child("solicitudes").orderByChild("alumno").equalTo(FirebaseAuth.getInstance().uid)
        }else{
            firebaseDatabase.reference
                    .child("solicitudes").orderByChild("profesor").equalTo(FirebaseAuth.getInstance().uid)
        }
        return RxFirebaseDatabase.observeValueEvent(ref)
    }

}