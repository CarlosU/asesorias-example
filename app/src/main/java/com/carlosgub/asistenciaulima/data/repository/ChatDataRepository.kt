package com.carlosgub.asistenciaulima.data.repository

import com.carlosgub.asistenciaulima.data.mapper.ChatMapper
import com.carlosgub.asistenciaulima.data.sources.firebase.ChatSourceFirebase
import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.domain.repository.ChatRepository
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import rx.Observable
import javax.inject.Inject

class ChatDataRepository @Inject constructor(private val source: ChatSourceFirebase) : ChatRepository {

    override fun getChat(type:String): Observable<List<Chat>> {
        return source.getChat(type)
                .map { ChatMapper.transformList(it) }
    }

    override fun getChatOneTime(type:String): Observable<List<Chat>> {
        return source.getChatOneTime(type)
                .map { ChatMapper.transformList(it) }
    }

    override fun getNewChat(idChat:String): Observable<String?> {
        return source.getNewChat(idChat)
                .map {
                    if(it.eventType==RxFirebaseChildEvent.EventType.ADDED){
                    it.value.value.toString()
                    }else{
                        null
                    }
                }
    }

    override fun sentMessage(idChat: String,message: String) {
        source.sentMessage(idChat,message)
    }

    override fun createChat(chat: Chat,callback:(Boolean)->Unit) {
        source.createChat(chat) {
            callback(it)
        }
    }

    override fun deleteChats(chatList: MutableList<Chat>, callback: (Boolean) -> Unit) {
        source.deleteChats(chatList) {
            callback(it)
        }
    }
}