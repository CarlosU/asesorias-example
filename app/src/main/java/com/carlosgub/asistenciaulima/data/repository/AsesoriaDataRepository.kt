package com.carlosgub.asistenciaulima.data.repository

import com.carlosgub.asistenciaulima.data.mapper.AsesoriaMapper
import com.carlosgub.asistenciaulima.data.mapper.PeticionAsesoriaMapper
import com.carlosgub.asistenciaulima.data.sources.firebase.AsesoriaSourceFirebase
import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.repository.AsesoriaRepository
import rx.Observable
import javax.inject.Inject

class AsesoriaDataRepository @Inject constructor(private val source: AsesoriaSourceFirebase) : AsesoriaRepository {

    override fun getAsesorias(uid:String): Observable<List<Asesoria>> {
        return source.getAsesorias(uid)
                .map { AsesoriaMapper.transformList(it) }
    }

    override fun deleteAsesorias(AsesoriasList:MutableList<PeticionAsesoria>, callback: (Boolean) -> Unit){
        source.deleteAsesorias(AsesoriasList){
            callback(it)
        }
    }
    override fun getPeticionesAsesoria(type:String) : Observable<List<PeticionAsesoria>>{
        return source.getPeticion(type)
                .map {
                    PeticionAsesoriaMapper.transformList(it)
                }
    }
}