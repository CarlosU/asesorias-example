package com.carlosgub.asistenciaulima.data.sources

import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides


@Module
class FirebaseModule {

    @Provides
    fun provideFirebaseDatabase(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }
}