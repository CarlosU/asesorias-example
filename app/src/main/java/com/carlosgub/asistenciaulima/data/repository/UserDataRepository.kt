package com.carlosgub.asistenciaulima.data.repository

import com.carlosgub.asistenciaulima.data.mapper.UserMapper
import com.carlosgub.asistenciaulima.data.sources.firebase.UserSourceFirebase
import com.carlosgub.asistenciaulima.domain.model.User
import com.carlosgub.asistenciaulima.domain.repository.UserRepository
import rx.Observable
import javax.inject.Inject

class UserDataRepository @Inject constructor(private val source: UserSourceFirebase) : UserRepository {

    override fun getUserType(uid:String): Observable<String> {
        return source.getUserType(uid)
                .map { it.value.toString() }
    }

    override fun getUser(uid: String): Observable<User> {
        return source.getUser(uid)
                .map {
                    val id = it.key?:""
                    val name = it.child("name").getValue(String::class.java)?:""
                    val type = it.child("type").getValue(String::class.java)?:""
                    val imageUrl = it.child("imageUrl").getValue(String::class.java)?:""
                    if(it.exists()){
                        User(name, type,id,true,imageUrl)
                    }else{
                        User(name, type,id,false,imageUrl)
                    }
                }
    }
}