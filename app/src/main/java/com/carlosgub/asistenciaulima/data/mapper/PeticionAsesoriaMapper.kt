package com.carlosgub.asistenciaulima.data.mapper

import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.carlosgub.asistenciaulima.domain.model.Documento
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.google.firebase.database.DataSnapshot

/**
 * @author Carlos Ugaz
 * @link :v.com
 */
object PeticionAsesoriaMapper {

    fun transformList(data: DataSnapshot) = data.children.map { transform(it) }

    private fun transform(data: DataSnapshot): PeticionAsesoria {
        val alumno = data.child("alumno").getValue(String::class.java)?:""
        val dia = data.child("dia").getValue(String::class.java)?:""
        val estado = data.child("estado").getValue(String::class.java)?:""
        val hora = data.child("hora").getValue(String::class.java)?:""
        val lugar = data.child("lugar").getValue(String::class.java)?:""
        val nameAlumno = data.child("nameAlumno").getValue(String::class.java)?:""
        val nameProfesor = data.child("nameProfesor").getValue(String::class.java)?:""
        val profesor = data.child("profesor").getValue(String::class.java)?:""
        val tema = data.child("tema").getValue(String::class.java)?:""
        val horaEpoch = data.child("horaEpoch").getValue(Long::class.java)?:0L
        val id = data.key?:""
        val estadoNumber = if(estado == "ENVIADO"){
            1
        }else if(estado == "ACEPTADO"){
            0
        }else if(estado == "RECHAZADO"){
            2
        }else if(estado == "TERMINADO"){
            3
        }else{
            4
        }

        var documentos:MutableList<Documento> = mutableListOf()
        data.child("archivos").children.forEach{documento->
            val name_archivo  = documento.child("name_archivo").getValue(String::class.java)?:""
            val size_archivo  = documento.child("size_archivo").getValue(String::class.java)?:""
            val tipo_archivo  = documento.child("tipo_archivo").getValue(String::class.java)?:""
            val url_archivo  = documento.child("url_archivo").getValue(String::class.java)?:""
            documentos.add(Documento(name_archivo,size_archivo,tipo_archivo,url_archivo))
        }
        val imageUrl = data.child("imageUrl").getValue(String::class.java)?:""
        return PeticionAsesoria(
                dia= dia,
                hora = hora,
                lugar = lugar,
                alumno = alumno,
                profesor = profesor,
                nameAlumno = nameAlumno,
                nameProfesor = nameProfesor,
                tema = tema,
                estado = estado,
                horaEpoch = horaEpoch,
                id = id,
                estadoNumber = estadoNumber,
                imageUrl = imageUrl ,
                documentos =  documentos
                )
    }

}