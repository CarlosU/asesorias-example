package com.carlosgub.asistenciaulima.data.mapper

import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.google.firebase.database.DataSnapshot

/**
 * @author Carlos Ugaz
 * @link :v.com
 */
object AsesoriaMapper {

    fun transformList(data: DataSnapshot) = data.children.map { transform(it) }

    private fun transform(data: DataSnapshot): Asesoria {
        val dia = data.child("dia").getValue(String::class.java)?:""
        val horaFin = data.child("horaFin").getValue(String::class.java)?:""
        val horaInicio = data.child("horaInicio").getValue(String::class.java)?:""
        val lugar = data.child("lugar").getValue(String::class.java)?:""
        val dayOfWeek = data.child("dayOfWeek").getValue(Int::class.java)?:0
        return Asesoria(dia, horaFin, horaInicio, lugar,dayOfWeek)
    }

}