package com.carlosgub.asistenciaulima.data.mapper

import com.carlosgub.asistenciaulima.domain.model.User
import com.google.firebase.database.DataSnapshot

/**
 * @author Carlos Ugaz
 * @link :v.com
 */
object UserMapper {

    fun transformList(data: DataSnapshot) = data.children.map { transform(it) }

    private fun transform(data: DataSnapshot): User {
        val id = data.key?:""
        val name = data.child("name").getValue(String::class.java)?:""
        val type = data.child("type").getValue(String::class.java)?:""
        val imageUrl = data.child("imageUrl").getValue(String::class.java)?:""
        return User(name = name,
                type = type,
                id = id,
                imageUrl = imageUrl)
    }

}