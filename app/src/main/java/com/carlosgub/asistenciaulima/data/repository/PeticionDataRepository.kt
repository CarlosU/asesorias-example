package com.carlosgub.asistenciaulima.data.repository

import com.carlosgub.asistenciaulima.data.mapper.ChatMapper
import com.carlosgub.asistenciaulima.data.mapper.PeticionAsesoriaMapper
import com.carlosgub.asistenciaulima.data.sources.firebase.PeticionSourceFirebase
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.repository.PeticionRepository
import rx.Observable
import javax.inject.Inject

class PeticionDataRepository @Inject constructor(private val source: PeticionSourceFirebase) : PeticionRepository {

    override fun enviarPeticionAsesoria(peticion: PeticionAsesoria, callback: () -> Unit) {
        source.enviarPeticionAsesoria(peticion){
            callback()
        }
    }

    override fun obtenerAsesorias(): Observable<List<PeticionAsesoria>> {
        return source.obtenerAsesorias()
                .map { PeticionAsesoriaMapper.transformList(it) }
    }
}