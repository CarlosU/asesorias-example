package com.carlosgub.asistenciaulima.data.sources.firebase

import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.FirebaseDatabase
import com.kelvinapps.rxfirebase.RxFirebaseChildEvent
import com.kelvinapps.rxfirebase.RxFirebaseDatabase
import rx.Observable
import javax.inject.Inject


class PeticionSourceFirebase @Inject constructor(private val firebaseDatabase: FirebaseDatabase) {

    fun enviarPeticionAsesoria(peticion: PeticionAsesoria, callback: () -> Unit) {
        val key = firebaseDatabase.reference
                .child("solicitudes").push().key!!
        firebaseDatabase.reference.child("solicitudes")
                .child(key).setValue(peticion).addOnSuccessListener {
                    callback()
                }
    }

    fun obtenerAsesorias() : Observable<DataSnapshot>{
        val ref = firebaseDatabase.reference
                    .child("solicitudes").orderByChild("alumno").equalTo(FirebaseAuth.getInstance().uid)
        return RxFirebaseDatabase.observeSingleValueEvent(ref)
    }
}