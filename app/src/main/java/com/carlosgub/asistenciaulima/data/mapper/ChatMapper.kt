package com.carlosgub.asistenciaulima.data.mapper

import com.carlosgub.asistenciaulima.domain.model.Chat
import com.google.firebase.database.DataSnapshot

/**
 * @author Carlos Ugaz
 * @link :v.com
 */
object ChatMapper {

    fun transformList(data: DataSnapshot) = data.children.map { transform(it) }

    private fun transform(data: DataSnapshot): Chat {
        val alumno = data.child("alumno").getValue(String::class.java)?:""
        val profesor = data.child("profesor").getValue(String::class.java)?:""
        val nameProfesor = data.child("nameProfesor").getValue(String::class.java)?:""
        val nameAlumno = data.child("nameAlumno").getValue(String::class.java)?:""
        val time = data.child("time").getValue(Long::class.java)?:0L
        val key=data.key?:""
        val list = ArrayList<String>()
        for(it in data.child("chat").children){
            list.add(it.value.toString())
        }
        val imageUrl = data.child("imageUrl").getValue(String::class.java)?:""
        return Chat(alumno, profesor,list,time,nameAlumno,nameProfesor,key,imageUrl)
    }

}