package com.carlosgub.asistenciaulima.data

import com.carlosgub.asistenciaulima.data.repository.*
import com.carlosgub.asistenciaulima.domain.repository.*
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds abstract fun bindChatRepository(repository: ChatDataRepository): ChatRepository
    @Binds abstract fun bindUserRepository(repository: UserDataRepository): UserRepository
    @Binds abstract fun bindAsesoriaRepository(repository: AsesoriaDataRepository): AsesoriaRepository
    @Binds abstract fun bindDatesRepository(repository: DateDataRepository): DateRepository
    @Binds abstract fun bindPeticionRepository(repository: PeticionDataRepository): PeticionRepository

}