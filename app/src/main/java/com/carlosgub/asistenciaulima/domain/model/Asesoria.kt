package com.carlosgub.asistenciaulima.domain.model

data class Asesoria (val dia: String= "",
                     val horaFin: String= "",
                     var horaInicio:String="",
                     val lugar:String="",
                     val dayOfWeek:Int=0)