package com.carlosgub.asistenciaulima.domain.repository

import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import rx.Observable

interface AsesoriaRepository {

    fun getAsesorias(uid:String): Observable<List<Asesoria>>
    fun deleteAsesorias(AsesoriasList:MutableList<PeticionAsesoria>,callback: (Boolean) -> Unit)
    fun getPeticionesAsesoria(type:String) : Observable<List<PeticionAsesoria>>
}