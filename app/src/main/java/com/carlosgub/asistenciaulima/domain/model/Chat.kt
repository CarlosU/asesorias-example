package com.carlosgub.asistenciaulima.domain.model

import java.io.Serializable

data class Chat (val alumno: String= "",
                 val profesor: String= "",
                 val chat:ArrayList<String>? = arrayListOf(),
                 val time:Long = 0L,
                 var nameAlumno:String="",
                 var nameProfesor:String="",
                 val id:String?="",
                 val imageUrl:String?=null) :Serializable