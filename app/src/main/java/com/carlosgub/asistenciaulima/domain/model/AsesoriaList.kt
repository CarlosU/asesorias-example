package com.carlosgub.asistenciaulima.domain.model

data class AsesoriaList (val dia: String= "",
                         val hora: String= "",
                         val lugar:String="",
                         val horaEpoch:Long)