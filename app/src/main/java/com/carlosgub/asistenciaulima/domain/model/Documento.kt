package com.carlosgub.asistenciaulima.domain.model

import java.io.Serializable

data class Documento (val name_archivo: String= "",
                      val size_archivo: String= "",
                      var tipo_archivo:String="",
                      val url_archivo:String="") :Serializable