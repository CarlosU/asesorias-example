package com.carlosgub.asistenciaulima.domain.repository

import com.carlosgub.asistenciaulima.domain.model.Chat
import rx.Observable

interface ChatRepository {

    fun getChat(type:String): Observable<List<Chat>>
    fun getChatOneTime(type:String): Observable<List<Chat>>
    fun getNewChat(idChat:String): Observable<String?>
    fun sentMessage(idChat: String,message:String)
    fun createChat(chat:Chat,callback:(Boolean)->Unit)
    fun deleteChats(chatList:MutableList<Chat>,callback: (Boolean) -> Unit)
}