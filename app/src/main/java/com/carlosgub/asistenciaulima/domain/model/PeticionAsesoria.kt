package com.carlosgub.asistenciaulima.domain.model

import java.io.Serializable

data class PeticionAsesoria (val dia: String= "",
                             val hora: String= "",
                             val lugar:String="",
                             val alumno: String= "",
                             val profesor: String= "",
                             var nameAlumno:String="",
                             var nameProfesor:String="",
                             var tema:String="",
                             var estado:String="Enviado",
                             var horaEpoch: Long,
                             var id:String?=null,
                             var estadoNumber:Int?=null,
                             val imageUrl:String?=null,
                             val documentos:MutableList<Documento>) : Serializable