package com.carlosgub.asistenciaulima.domain.usecases

import com.carlosgub.asistenciaulima.domain.repository.DateRepository
import javax.inject.Inject

class GetDateUseCase @Inject constructor(private val rep: DateRepository) {

    fun getDates(callback:(MutableList<Long>) -> Unit){
        rep.getDates{
            callback(it)
        }
    }
}