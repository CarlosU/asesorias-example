package com.carlosgub.asistenciaulima.domain.repository

import com.carlosgub.asistenciaulima.domain.model.User
import rx.Observable

interface UserRepository {

    fun getUserType(uid:String): Observable<String>
    fun getUser(uid:String): Observable<User>
}