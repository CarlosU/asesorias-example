package com.carlosgub.asistenciaulima.domain.repository

import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import rx.Observable

interface PeticionRepository {

    fun enviarPeticionAsesoria(peticion: PeticionAsesoria, callback:()->Unit)
    fun obtenerAsesorias(): Observable<List<PeticionAsesoria>>
}