package com.carlosgub.asistenciaulima.domain.usecases

import com.carlosgub.asistenciaulima.domain.model.Chat
import com.carlosgub.asistenciaulima.domain.repository.ChatRepository
import rx.Observable
import javax.inject.Inject

class GetChatListUseCase @Inject constructor(private val rep: ChatRepository) {

    fun getChats(type:String): Observable<List<Chat>> {
        return rep.getChat(type)
                .map {
                    val historialList = mutableListOf<Chat>()
                    historialList.addAll(it)
                    historialList.sortedByDescending { it.time }
                }
    }

    fun getChatsOneTime(type:String): Observable<List<Chat>> {
        return rep.getChatOneTime(type)
                .map {
                    val historialList = mutableListOf<Chat>()
                    historialList.addAll(it)
                    historialList.sortedByDescending { it.time }
                }
    }

    fun getNewChat(idChat:String):Observable<String?>{
        return rep.getNewChat(idChat)
    }

    fun sentMessage(idChat: String,message:String){
        rep.sentMessage(idChat,message)
    }

    fun createChat(chat:Chat,callback:(Boolean)->Unit){
        rep.createChat(chat){
            callback(it)
        }
    }

    fun deleteChats(chatList:MutableList<Chat>,callback: (Boolean) -> Unit){
        rep.deleteChats(chatList){
            callback(it)
        }
    }
}