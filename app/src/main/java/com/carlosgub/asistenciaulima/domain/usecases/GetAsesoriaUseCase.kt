package com.carlosgub.asistenciaulima.domain.usecases

import com.carlosgub.asistenciaulima.domain.model.Asesoria
import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.repository.AsesoriaRepository
import rx.Observable
import javax.inject.Inject

class GetAsesoriaUseCase @Inject constructor(private val rep: AsesoriaRepository) {

    fun getAsesorias(uid:String): Observable<List<Asesoria>>  {
        return rep.getAsesorias(uid)
    }

    fun deleteAsesorias(AsesoriasList:MutableList<PeticionAsesoria>,callback: (Boolean) -> Unit){
        rep.deleteAsesorias(AsesoriasList){
            callback(it)
        }
    }

    fun getPeticionesAsesoria(type:String):Observable<List<PeticionAsesoria>>{
        return rep.getPeticionesAsesoria(type)
    }
}