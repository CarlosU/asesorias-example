package com.carlosgub.asistenciaulima.domain.model

import java.io.Serializable

data class User (val name: String= "",
                 val type: String= "",
                 val id:String="",
                 val exist:Boolean=false,
                 val imageUrl:String?=null) :Serializable