package com.carlosgub.asistenciaulima.domain.repository

import com.carlosgub.asistenciaulima.domain.model.Asesoria
import rx.Observable

interface DateRepository {

    fun getDates(callback:(MutableList<Long>) -> Unit)
}