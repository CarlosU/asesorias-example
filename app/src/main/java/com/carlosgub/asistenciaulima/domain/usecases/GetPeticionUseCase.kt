package com.carlosgub.asistenciaulima.domain.usecases

import com.carlosgub.asistenciaulima.domain.model.PeticionAsesoria
import com.carlosgub.asistenciaulima.domain.repository.PeticionRepository
import rx.Observable
import javax.inject.Inject

class GetPeticionUseCase @Inject constructor(private val rep: PeticionRepository) {

    fun enviarPeticionAsesoria(peticion: PeticionAsesoria, callback:()->Unit){
        return rep.enviarPeticionAsesoria(peticion){
            callback()
        }
    }

    fun obtenerAsesorias(): Observable<List<PeticionAsesoria>>{
        return rep.obtenerAsesorias()
                .map {
                    val historialList = mutableListOf<PeticionAsesoria>()
                    historialList.addAll(it)
                    historialList.sortedWith(compareBy(PeticionAsesoria::horaEpoch))
                }
    }
}