package com.carlosgub.asistenciaulima.domain.usecases

import com.carlosgub.asistenciaulima.domain.model.User
import com.carlosgub.asistenciaulima.domain.repository.UserRepository
import rx.Observable
import javax.inject.Inject

class GetUserUseCase @Inject constructor(private val rep: UserRepository) {

    fun getUserType(uid:String): Observable<String>  {
        return rep.getUserType(uid)
    }

    fun getUser(uid:String): Observable<User>  {
        return rep.getUser(uid)
    }



}