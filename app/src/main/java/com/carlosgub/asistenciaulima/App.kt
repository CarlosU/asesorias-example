package com.carlosgub.asistenciaulima

import com.google.firebase.database.FirebaseDatabase
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

open class App : DaggerApplication(){

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        return DaggerAppComponent.builder().create(this)
    }

}