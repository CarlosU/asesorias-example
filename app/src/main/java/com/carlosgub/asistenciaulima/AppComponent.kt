package com.carlosgub.asistenciaulima


import com.carlosgub.asistenciaulima.data.DataModule
import com.carlosgub.asistenciaulima.data.sources.FirebaseModule
import com.carlosgub.asistenciaulima.presentation.BindingModule
import com.carlosgub.asistenciaulima.presentation.BuildersModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


/**
 * @author CarlosU
 * @link github.com/carlosgub
 */
@Component(modules = arrayOf(
        AppModule::class,
        DataModule::class,
        BuildersModule::class,
        FirebaseModule::class,
        BindingModule::class,
        AndroidSupportInjectionModule::class))

interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()

}