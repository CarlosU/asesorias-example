package com.example.carlosgub.dispensadodecomida.utils.extensions

import android.util.Log
import java.text.DateFormatSymbols
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Kevin Salazar
 * @link kevicsalazar.com
 */

val TIME_ZONE = "America/Lima"
val TIME_ZONE_PERU get() = Locale("es", "PE")

/******************************************************************
 * Format Date Methods
 */

val F1 get() = SimpleDateFormat("dd/MM/yyyy", TIME_ZONE_PERU)
val F2 get() = SimpleDateFormat("yyyy-MM-dd", TIME_ZONE_PERU)
val F3 get() = SimpleDateFormat("dd/MM/yyyy HH:mm", TIME_ZONE_PERU)
val F4 get() = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", TIME_ZONE_PERU)
val F5 get() = SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss Z", Locale.ENGLISH)
val F6 get() = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", TIME_ZONE_PERU)
val F7 get() = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'", TIME_ZONE_PERU)
val F8 get() = SimpleDateFormat("EEEE d 'de' MMMM", TIME_ZONE_PERU)
val F9 get() = SimpleDateFormat("EEEE d", TIME_ZONE_PERU)
val F10 get() = SimpleDateFormat("HH:mm", TIME_ZONE_PERU)
val F11 get() = SimpleDateFormat("h a", TIME_ZONE_PERU)
val F12 get() = SimpleDateFormat("h:mm a", TIME_ZONE_PERU)
val F13 get() = SimpleDateFormat("EEEE dd MMMM", TIME_ZONE_PERU)
val F14 get() = SimpleDateFormat("EEEE d 'de' MMMM,yyyy", TIME_ZONE_PERU)
val F15 get() = SimpleDateFormat("d 'de' MMMM", TIME_ZONE_PERU)
val F16 get() = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", TIME_ZONE_PERU)

fun now(): Date = Date()

fun today(): Date = Date().truncate()

typealias TimeInterval = Long

val TimeIntervalMinute: TimeInterval = 60
val TimeIntervalHour: TimeInterval = 3600
val TimeIntervalDay: TimeInterval = 86400
val TimeIntervalWeek: TimeInterval = 604800
val TimeIntervalYear: TimeInterval = 31556926

val Calendar.year get() = get(Calendar.YEAR)
val Calendar.month get() = get(Calendar.MONTH)
val Calendar.weekOfYear get() = get(Calendar.WEEK_OF_YEAR)
val Calendar.dayOfYear get() = get(Calendar.DAY_OF_YEAR)
val Calendar.dayOfMonth get() = get(Calendar.DAY_OF_MONTH)
val Calendar.dayOfWeek get() = get(Calendar.DAY_OF_WEEK)
val Calendar.hour get() = get(Calendar.HOUR)
val Calendar.hourOfDay get() = get(Calendar.HOUR_OF_DAY)
val Calendar.minute get() = get(Calendar.MINUTE)
val Calendar.second get() = get(Calendar.SECOND)
val Calendar.lastDayOfMonth get() = getActualMaximum(Calendar.DAY_OF_MONTH)

val Date.isoDateFormat get() = "yyyy-MM-dd'T'HH:mm:ssZ"
const val dateFormat1 = "dd ' de ' MMMM ' de ' yyyy"
const val dateFormat2 = "EEEE, d ' de ' MMMM ' de ' yyyy"

val Date.timeInSeconds get() = time / 1000

/**************************************************************************************************
 * MARK: String Date Methods
 **************************************************************************************************/

fun Date.string(format: String, locale: String? = null): String? {
    val df = SimpleDateFormat(format, locale?.let { Locale(it) } ?: Locale("es"))
    return df.format(this)
}

fun String.toDate(format: String, locale: String? = null): Date? {
    val df = SimpleDateFormat(format, locale?.let { Locale(it) } ?: Locale("es"))
    return try {
        df.parse(this)
    } catch (e: Exception) {
        e.printStackTrace()
        null
    }
}

fun Date.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar
}

/**************************************************************************************************
 * MARK: Comparing Date Methods
 **************************************************************************************************/

fun Date.isSameDayAsDate(date: Date): Boolean {
    val components1 = toCalendar()
    val components2 = date.toCalendar()
    return ((components1.year == components2.year) &&
            (components1.month == components2.month) &&
            (components1.dayOfMonth == components2.dayOfMonth))
}

fun Date.isToday(): Boolean = isSameDayAsDate(now())

fun Date.isTomorrow(): Boolean = isSameDayAsDate(now().dateByAddingDays(1))

fun Date.isYesterday(): Boolean = isSameDayAsDate(now().dateBySubtractingDays(1))

fun Date.isSameWeekAsDate(date: Date): Boolean {
    val components1 = toCalendar()
    val components2 = date.toCalendar()
    // Must be same week. 12/31 and 1/1 will both be week "1" if they are in the same week
    if (components1.weekOfYear != components2.weekOfYear) {
        return false
    }
    // Must have a time interval under 1 week. Thanks @aclark
    return timeIntervalSince(date) < TimeIntervalWeek
}

fun Date.isThisWeek(): Boolean = isSameWeekAsDate(now())

fun Date.isNextWeek(): Boolean = isSameWeekAsDate(now().dateByAddingDays(7))

fun Date.isLastWeek(): Boolean = isSameWeekAsDate(now().dateBySubtractingDays(7))

fun Date.isSameMonthAsDate(date: Date): Boolean {
    val components1 = toCalendar()
    val components2 = date.toCalendar()
    return ((components1.month == components2.month) &&
            (components1.year == components2.year))
}

fun Date.isThisMonth(): Boolean = isSameMonthAsDate(now())

fun Date.isSameYearAsDate(date: Date): Boolean {
    val components1 = toCalendar()
    val components2 = date.toCalendar()
    return (components1.year == components2.year)
}

fun Date.isThisYear(): Boolean = isSameYearAsDate(now())

fun Date.isEarlierThanDate(date: Date): Boolean = compareTo(date) == -1

fun Date.isLaterThanDate(date: Date): Boolean = compareTo(date) == 1

fun Date.isBetweenDates(dateStart: Date, dateEnd: Date, including: Boolean = false): Boolean {
    if (including && isSameDayAsDate(dateStart)) {
        return true
    }
    if (including && isSameDayAsDate(dateEnd)) {
        return true
    }
    return isLaterThanDate(dateStart) && isEarlierThanDate(dateEnd)
}

fun Date.isBetweenDays(dateStart: Date, dateEnd: Date, including: Boolean = false): Boolean {
    if (isSameDayAsDate(dateStart)) {
        return including
    }
    if (isSameDayAsDate(dateEnd)) {
        return including
    }
    return isLaterThanDate(dateStart) && isEarlierThanDate(dateEnd)
}

fun Date.isInPast(): Boolean = isEarlierThanDate(now())

fun Date.isInFuture(): Boolean = isLaterThanDate(now())

/**************************************************************************************************
 * MARK: Adjusting Date Methods
 **************************************************************************************************/

fun Date.truncate(): Date {
    with(toCalendar()) {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
        return time
    }
}

fun Date.dateByAddingDays(days: Int): Date {
    val timeInterval = timeInSeconds + TimeIntervalDay * days
    return Date(timeInterval * 1000L)
}

fun Date.dateBySubtractingDays(days: Int): Date = dateByAddingDays(-days)

fun Date.dateByAddingHours(hours: Int): Date {
    val timeInterval = timeInSeconds + TimeIntervalHour * hours
    return Date(timeInterval * 1000L)
}

fun Date.dateBySubtractingHours(hours: Int): Date = dateByAddingHours(-hours)

fun Date.dateByAddingMinutes(minutes: Int): Date {
    val timeInterval = timeInSeconds + TimeIntervalMinute * minutes
    return Date(timeInterval * 1000L)
}

fun Date.dateBySubtractingMinutes(minutes: Int): Date = dateByAddingMinutes(-minutes)

fun Date.dateByAddingSeconds(seconds: Int): Date {
    val timeInterval = timeInSeconds + seconds
    return Date(timeInterval * 1000L)
}

fun Date.dateBySubtractingSeconds(seconds: Int): Date = dateByAddingSeconds(-seconds)

fun Date.timeIntervalSince(date: Date): Long = (time - date.time) / 1000

fun Date.toStartOfWeek(): Date {
    with(toCalendar()) {
        set(Calendar.DAY_OF_WEEK, 1)
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
        return time
    }
}

/**************************************************************************************************
 * MARK: Retrieving Intervals Date Methods
 **************************************************************************************************/

fun Date.secondsAfterNow(): Int = secondsAfterDate(now())

fun Date.secondsBeforeNow(): Int = secondsBeforeDate(now())

fun Date.secondsAfterDate(date: Date): Int {
    val timeInterval = timeIntervalSince(date)
    return timeInterval.toInt()
}

fun Date.secondsBeforeDate(date: Date): Int {
    val timeInterval = date.timeIntervalSince(this)
    return timeInterval.toInt()
}

fun Date.minutesAfterDate(date: Date): Int {
    val timeInterval = timeIntervalSince(date)
    return (timeInterval / TimeIntervalMinute).toInt()
}

fun Date.minutesBeforeDate(date: Date): Int {
    val timeInterval = date.timeIntervalSince(this)
    return (timeInterval / TimeIntervalMinute).toInt()
}

fun Date.hoursAfterDate(date: Date): Int {
    val timeInterval = timeIntervalSince(date)
    return (timeInterval / TimeIntervalHour).toInt()
}

fun Date.hoursBeforeDate(date: Date): Int {
    val timeInterval = date.timeIntervalSince(this)
    return (timeInterval / TimeIntervalHour).toInt()
}

fun Date.daysAfterDate(date: Date): Int {
    val timeInterval = timeIntervalSince(date)
    return (timeInterval / TimeIntervalDay).toInt()
}

fun Date.daysBeforeDate(date: Date): Int {
    val timeInterval = date.timeIntervalSince(this)
    return (timeInterval / TimeIntervalDay).toInt()
}

fun Date.changeDate(year: Int, month: Int, dayOfMonth: Int): Date {
    with(toCalendar()) {
        set(Calendar.YEAR, year)
        set(Calendar.MONTH, month)
        set(Calendar.DAY_OF_MONTH, dayOfMonth)
        return time
    }
}

fun Date.changeDay(dayOfMonth: Int): Date {
    with(toCalendar()) {
        set(Calendar.DAY_OF_MONTH, dayOfMonth)
        return time
    }
}

fun Date.dateByManipulateYears(nYears: Int): Date {
    with(toCalendar()) {
        set(Calendar.YEAR, nYears)
        return time
    }
}

fun Date.calculateAge(): Int {
    val today = now().toCalendar()
    val birth = toCalendar()
    birth.time = this

    var yearDifference = today.year - birth.year

    if (today.month < birth.month) yearDifference--
    else {
        if (today.month == birth.month && today.dayOfMonth < birth.dayOfMonth) yearDifference--
    }

    return yearDifference
}

fun getMonthNameByNumber(num: Int): String {
    var month = "wrong"
    val dfs = DateFormatSymbols(TIME_ZONE_PERU)
    val months = dfs.months
    if (num in 0..11) month = months[num]
    return month
}

fun getWeekDayNameByNumber(num: Int): String {
    var day = "wrong"
    val dfs = DateFormatSymbols(TIME_ZONE_PERU)
    val months = dfs.weekdays
    if (num in 1..7) day = months[num]
    return day
}