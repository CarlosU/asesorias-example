package com.carlosgub.asistenciaulima.utils.notifications

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.iid.FirebaseInstanceId



class FCMInstanceIDService: FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()

        val token = FirebaseInstanceId.getInstance().token
        Log.d(":)", "Refreshed token: " + token!!)
        sendTokenToCreateArnAWS(token)
    }

    private fun sendTokenToCreateArnAWS(token: String?) {

        //here you can send the token to AWS server or send to your own server.

    }
}