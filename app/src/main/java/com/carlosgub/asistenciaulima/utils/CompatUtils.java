package com.carlosgub.asistenciaulima.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.transition.Transition;
import android.view.Window;

/**
 * Created by Kevin.
 */
@SuppressWarnings("all")
public class CompatUtils {

    public static void setExitTransition(Window window, Transition transition) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setExitTransition(transition);
        }
    }

    public static void setEnterTransition(Window window, Transition transition) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setEnterTransition(transition);
        }
    }

    public static void setStatusBarColor(Window window, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(color);
        }
    }

    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE);
        } else {
            return Html.fromHtml(source);
        }
    }

}
