package com.carlosgub.asistenciaulima.utils.dialogs

import android.content.DialogInterface
import android.support.design.widget.TextInputEditText
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.carlosgub.asistenciaulima.R
import com.carlosgub.asistenciaulima.utils.textInputEditText
import com.carlosgub.asistenciaulima.utils.textInputLayout
import org.jetbrains.anko.*

class FeedbackDialog(ui: AnkoContext<View>) {

    lateinit var dialog: DialogInterface
    lateinit var feedbackText: TextInputEditText
    lateinit var cancelButton: TextView
    lateinit var okButton: TextView

    init {
        with(ui) {
            dialog = alert {

                customView {
                    verticalLayout {
                        padding = dip(10)

                        textView("Petición de Asesoría") {
                            textSize = 24f
                            textColor = context.resources.getColor(R.color.black)
                        }.lparams {
                            bottomMargin = dip(10)
                        }

                        textView("Llene el siguiente campo para realizar la petición de la Asesoría").lparams {
                            bottomMargin = dip(10)
                        }

                        textInputLayout {
                            hint = "Tema a conversar"
                            feedbackText = textInputEditText {
                                textSize = 16f
                            }
                        }

                        linearLayout {
                            topPadding = dip(24)
                            orientation = LinearLayout.HORIZONTAL
                            horizontalGravity = Gravity.END

                            cancelButton = textView("Cancelar") {
                                textSize = 14f
                                textColor = context.resources.getColor(R.color.colorPrimary)
                            }.lparams {
                                marginEnd = dip(10)
                            }

                            okButton = textView("Enviar") {
                                textSize = 14f
                                textColor = context.resources.getColor(R.color.colorPrimary)
                            }
                        }
                    }
                }
            }.show()
        }
    }
}