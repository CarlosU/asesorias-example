package com.carlosgub.asistenciaulima.presentation.views


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.carlosgub.asistenciaulima.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class EnviarSolicitudTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun enviarSolicitudTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        /*val textInputEditText3 = onView(
                allOf(withId(R.id.email),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText3.perform(replaceText("20131349@aloe.ulima.edu.pe"), closeSoftKeyboard())

        val textInputEditText13 = onView(
                allOf(withId(R.id.email), withText("20131349@aloe.ulima.edu.pe"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText13.perform(closeSoftKeyboard())

        val textInputEditText14 = onView(
                allOf(withId(R.id.password),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))

        textInputEditText14.perform(replaceText("20131349"), closeSoftKeyboard())

        val textInputEditText15 = onView(
                allOf(withId(R.id.password), withText("20131349"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))
        textInputEditText15.perform(pressImeActionButton())

        val linearLayout = onView(
                allOf(withId(R.id.btLogin),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()))
        linearLayout.perform(click())*/

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        val cardView = onView(
                allOf(withId(R.id.cardViewIC),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvFC),
                                        0),
                                0),
                        isDisplayed()))
        cardView.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        val actionMenuItemView = onView(
                allOf(withId(R.id.info), withContentDescription("informacion del docente"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbarAC),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        val linearLayout2 = onView(
                allOf(withId(R.id.llAsesoriasAI),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("android.widget.LinearLayout")),
                                        2),
                                0),
                        isDisplayed()))
        linearLayout2.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        val linearLayout3 =  onView(
                allOf(withId(R.id.cardViewIAA),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvAA),
                                        0),
                                0),
                        isDisplayed()))
        linearLayout3.perform(longClick())

        val textInputEditText16 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withClassName(`is`("android.support.design.widget.TextInputLayout")),
                                0),
                        0),
                        isDisplayed()))
        textInputEditText16.perform(replaceText("Prueba de solicitud"), closeSoftKeyboard())

        val textView = onView(
                allOf(withText("Enviar"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(`is`("org.jetbrains.anko._LinearLayout")),
                                        3),
                                1),
                        isDisplayed()))
        textView.perform(click())

        Thread.sleep(5000)
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
