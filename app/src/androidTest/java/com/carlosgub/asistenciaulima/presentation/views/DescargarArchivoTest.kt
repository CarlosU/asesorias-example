package com.carlosgub.asistenciaulima.presentation.views


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.longClick
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.carlosgub.asistenciaulima.R
import kotlinx.android.synthetic.main.activity_detalle.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class DescargarArchivoTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun descargarArchivoTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        /*Thread.sleep(3000)


        val textInputEditText3 = onView(
                allOf(withId(R.id.emailAL),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()))
        textInputEditText3.perform(replaceText("20131349@aloe.ulima.edu.pe"), closeSoftKeyboard())

        val textInputEditText18 = onView(
                allOf(withId(R.id.passwordAL),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()))
        textInputEditText18.perform(replaceText("20131349"), closeSoftKeyboard())

        val linearLayout = onView(
                allOf(withId(R.id.btLoginAL),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()))
        linearLayout.perform(click())*/

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(3000)

        val bottomNavigationItemView = onView(
                allOf(withId(R.id.asesorias), withContentDescription("Asesorias"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.bottomNavigationAM),
                                        0),
                                1),
                        isDisplayed()))
        bottomNavigationItemView.perform(click())

        val cardView = onView(
                allOf(withId(R.id.cardViewIA),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvAsesoriasFA),
                                        0),
                                0),
                        isDisplayed()))
        cardView.perform(click())

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        Thread.sleep(2000)

        val linearLayout3 =  onView(
                allOf(withId(R.id.cardViewII),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.rvAD),
                                        0),
                                0),
                        isDisplayed()))
        linearLayout3.perform(longClick())
        Thread.sleep(2000)
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
